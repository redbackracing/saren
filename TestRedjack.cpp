#include <Redjack.hpp>
#include <ExceptionLogger.hpp>
#include <iostream>
#include <string>
#include <map>

using namespace std;

int main(){
  
  FIFOPtr input(new FIFO());
  FIFOPtr output(new FIFO());
  ConfigModulePtr dut(new Redjack("Redjack",input, output));
  ExceptionLogger& _logger = ExceptionLogger::getInstance(); 
  
  // Currently, only tests for one channel - channel 1 on expansion board
  dut->setConfigFileName("Redjack_oneChannel");
  string s;
	int _backLog, _channelNum, _packetCounter = 0;
  int j = 0;
  
  RedjackPtr RedjackConfigModule = std::dynamic_pointer_cast<Redjack>(dut); 
  if(dut->startModule()){
    cout << "Redjack started..." << endl;
  }
  
  std::map<int, float> sampleMap = RedjackConfigModule->getSamplingRateMap();
  if(sampleMap.empty()){
    std::cout << "Map is empty" << endl;
  }
  
  while(dut->getRunning()){
    SamplePtr _sample = output->getNextValue();
    // Can be omitted for better processing
    cout << "channel Number: " << _sample->getChannel() << " Channel Value: " << _sample->getValue() << " Channel Time: " << _sample->getTime() << endl;
    // Currently terminates after 50 samples - however, sampling rate is set in
    // .rbrc file
    if( j == 100 ){
      dut->stopModule();
      cout << "Redjack Stopped." << endl;
    }
    ++j;
  }
 	return 0;
		
}
