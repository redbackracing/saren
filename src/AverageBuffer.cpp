#include <AverageBuffer.hpp>

SamplePtr AverageBuffer::push(SamplePtr _in){

  _sum += _in->getValue();

  return Buffer::push(std::move(_in));
}

SamplePtr AverageBuffer::pop(){
  SamplePtr result = Buffer::pop();

  if(result.get() != NULL){
    _sum -= result->getValue();  
  }

  return std::move(result);
}

SamplePtr AverageBuffer::averageSample(){
  SamplePtr result;

  pthread_mutex_lock(&_mutex);

  result.reset(new Sample(0,0,0));

  //Check upper and lower bounds are valid
  if(_buffer[0].get() != NULL && _buffer[_count-1].get() != NULL){
    result->setChannel(_buffer[0]->getChannel());

    result->setValue(_sum/_count);

    result->setTime(_buffer[0]->getTime() + (_buffer[_count-1]->getTime() - _buffer[0]->getTime())/2);  
  }

  pthread_mutex_unlock(&_mutex);

  Buffer::clear();

  _sum = 0;

  return result;
}