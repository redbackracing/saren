#ifndef FILE_LOGGER_HEADER
#define FILE_LOGGER_HEADER

#include <cstdio>
#include <string>
#include <cstdint>
#include <vector>
#include <memory>

#include <FilePtr.hpp>

class FileLogger {
private:
  FilePtr _file;
  bool _isBinary;
       
public:
  /*
   * Constructs the file logger
   * Takes the filename and if binary output is expected as arguments
   */
  FileLogger(const std::string& filename, bool isBinary = false, bool override = false);
  ~FileLogger();
  /*
   * Write methods
   * One for non-binary data,
   * one for binary
   * Returns if write was successful or not
   */
  bool write(const std::string& data);
  bool write(const std::vector<uint8_t>& bytes);
        
        
};

#endif
