#ifndef SAMPLE_HEADER
#define SAMPLE_HEADER

#include <cstdint>
#include <memory>

//Union field for channel, value and time
typedef union _union {
  uint32_t integer;
  float decimal;
  std::array<uint8_t,4> bytes;
}SampleField;

//Underlying container logic for Sample
class Sample {
public:
	Sample();
  Sample(const uint32_t& channel, const float& value, const float& acqTime, const bool diag = false, const bool warn = false, const std::string name = "");

  void toString(std::string& output);
  std::string toString();
  void toJSONString2(std::string& output);
  void toJSONString(std::string& output);
  std::string toJSONString();
  
  bool equals(Sample* rhs);

  //Getters & Setters
  std::string getName();
  void setName(const std::string& name);
	bool equals(Sample& rhs);

	//Getters & Setters
	uint32_t getChannel();
	void setChannel(const uint32_t& channel);

  float getValue();
  void setValue(const float& value);

  float getTime();
  void setTime(const float& acqTime);

	bool getDiag();
	void setDiag(const bool& diag);

	bool getWarn();
	void setWarn(const bool& warn);


private:
  std::string _name;
  SampleField _channel;
  SampleField _value;
  SampleField _time;

  bool _diag;
  bool _warn;

  void round(float& ioFloat);

};


//Unique pointer implementation for Samples, to ensure resource ownership is done correctly
typedef std::unique_ptr<Sample> SamplePtr;

#endif
