//NEW
#include <FileWriter.hpp>
#include <cstdint>
#include <vector>
#include <ExceptionLogger.hpp>
#include <iostream>
#include <ctime>

FileWriter::FileWriter(const std::string& name, FIFOPtr input, FIFOPtr output, bool Ybinary):Module(name,input,output){
	
 	
  _logger.reset(new FileLogger(getTimestampfilename(),Ybinary,true));//true 1 means yea binary , true 2 means yes to override old data
}

FileWriter::~FileWriter(){};

SamplePtr FileWriter::process(SamplePtr current){
  std::string writeBuffer;
  current->toJSONString2((writeBuffer));
  if(!(_logger->write(writeBuffer))){
    _exceptionLogger.writeLog("FileWriter - Data write out didn't work");
  }
  writeBuffer.clear();
  current.reset();
  return std::move(current);
};

std::string getTimestampfilename(void){
  time_t rawtime;									// time obj
	struct tm *timeinfo;						// calendar time broken into components

	time(&rawtime);
	timeinfo = localtime(&rawtime);
  std::string filename = asctime(timeinfo);	// convert struct info human readable string

  for(int i=0; i<filename.size();++i){
    if(filename[i] == ' '){
      filename[i]='-';
    }
  }

  filename[16] = '-';
  for(int i=17; i< filename.size(); ++i){
		filename[i]=filename[i+2];
  }
  filename[22]='.';
  filename[23]='r';
  filename[24]='b';
  filename[25]='r';
  filename[26]='d';
  filename[27]='\0';
 
  return filename;
}
