#include <FIFO.hpp>

FIFO::FIFO(size_t capacity /*= 20*/){
  _size = capacity;
  _count = 0;

  pthread_mutex_init(&_mutex,NULL);
  pthread_cond_init(&_notFull,NULL);
  pthread_cond_init(&_notEmpty,NULL);

}

FIFO::~FIFO(){
  _buffer.clear();
  pthread_mutex_destroy(&_mutex);
  pthread_cond_destroy(&_notEmpty);
  pthread_cond_destroy(&_notFull);
}

void FIFO::addValue(SamplePtr insert){
  pthread_mutex_lock(&_mutex);
  
  //If the buffer is full, wait till it is not full
  //Release mutex for use
	if(_count >= _size){
		pthread_cond_wait(&_notFull,&_mutex);
	}
	
  //Adds SamplePtr to buffer via move
  _buffer.push_back(std::move(insert));
  _count++;

  //Signal the buffer is not empty anymore
  //Release mutex for other threads
  pthread_cond_signal(&_notEmpty);
  pthread_mutex_unlock(&_mutex);
}

SamplePtr FIFO::getNextValue(){
  SamplePtr temp;

  pthread_mutex_lock(&_mutex);

  //If the buffer is empty, wait till it is not empty
  if(_count <= 0){
  	pthread_cond_wait(&_notEmpty,&_mutex);
  }
  
  //Removes SamplePtr from buffer by first moving the pointer
  //then erasing the SamplePtr container itself
  temp = std::move(_buffer.front());
  _buffer.erase(_buffer.begin());
  _count--;

  pthread_cond_signal(&_notFull);
  pthread_mutex_unlock(&_mutex);

  return std::move(temp);
}

size_t FIFO::getCapacity(){
  //No mutex as value never changes
  return _size;
}


size_t FIFO::getSize(){
  size_t result = 0;

  //Function requires mutex lock as it is used by multiple threads
  pthread_mutex_lock(&_mutex);

  result = _count;
  
  pthread_mutex_unlock(&_mutex);

  return result;
}
