#ifndef CONFIG_FILE_PARSER_HEADER
#define CONFIG_FILE_PARSER_HEADER

#include <bitset>
#include <fstream>

#include <ConfigFile.hpp>
#include <json.h>
#include <FilePtr.hpp>

// Enum which describes the error codes, each represents a power of two
enum error_codes {BAD_FILE = 1, BAD_JSON = 2, BAD_INPUT = 4, BAD_OUTPUT = 8};

class ConfigFileParser {
public:
  /* Parses a configuration file given a filename
   * The ConfigFile structure is passed in a reference to populate
   * Returns 0 if successful, other error 
   * exception string can be found in logs or retrieved by matchError function
   */
  unsigned long parseConfig(const std::string& iFilename, ConfigFile& oFile);

  /* Given a ConfigFileParser error code, 
   * returns the associated string
   */
  static std::string matchError(const unsigned long& errorCode);

  /* Given a filename, without prefix or suffix i.e. config/name.rbrc should just be 'name'
   * Returns true if file is open and ready in stream
   */
  bool readFile(const std::string& iFilename, std::ifstream& oFileStream);
  
  /* Given a valid FilePtr, it reads the JSON within the file
   * Returns true if JSON is valid and readable, and the JSON is returned by reference to oValue
   * Returns false if JSON isn't valid, and oValue will be empty
   */
  bool readJSON(std::ifstream& iFile, Json::Value& oValue);
  
  /* Given a non-empty Json value, the function attempts to create the input map
   * The map links a channel number to a ConfigInput structure
   * If invalid in any way, the oInputMap will be empty and this will return false
   */
  bool readInputMap(const Json::Value& iRoot, ConfigInputMap& oInputMap);
  
  /* Given a non-empty Json value, the function attempts to create the input channel
   * If invalid in any way, the oInput will be empty and this will return false
   */
  bool readInput(const Json::Value& iRoot, ConfigFileInput& oInput);
  
  /* Given a non-empty Json value, the function attempts to create the Output section
   * If invalid in any way, the oOutput will be empty and this will return false
   */
  bool readOutput(const Json::Value& iRoot, ConfigFileOutput& oOutput);

private:

  //JSON Helper functions
  bool readVector(const Json::Value& iRoot, std::vector<int>& oOutput);
  void readRange(const Json::Value& iRoot, Range& oOutput);
  void readConversion(const Json::Value& iRoot, std::vector<Point>& oPoint);
  int readInt(const Json::Value& iRoot);
  float readFloat(const Json::Value& iRoot);

  std::bitset<4> _error;

};

#endif
