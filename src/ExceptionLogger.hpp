#ifndef EXCEPTION_LOGGER_HEADER
#define EXCEPTION_LOGGER_HEADER

#include <FileLogger.hpp>
#include <string>
#include <sstream>
#include <pthread.h>
#include <memory>

class ExceptionLogger {
    private:
        std::unique_ptr<FileLogger> _logger;
        
        //To handle write race conditions
        pthread_mutex_t _mutex;
        
        //Uses FileLogger's write functions in a 
        //asynchronous manner
        void write(const std::string& logLine);
        
    public:
        static ExceptionLogger& getInstance();
        void writeLog(const std::string& logLine);
        void writeLog(const char* logLine);
        ExceptionLogger();
        ~ExceptionLogger();
};

#endif
