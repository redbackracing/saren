#include <Buffer.hpp>
#include <ExceptionLogger.hpp>

Buffer::~Buffer(){
}

bool Buffer::isFull(){
  return (_count == _size);
}

SamplePtr Buffer::push(SamplePtr _in){
  SamplePtr result;

  // If SamplePtr is NULL, return it without pushing
  if(_in.get() == NULL){
    return std::move(_in);
  } 

  // Checks if buffer is full
  if(isFull()){
    result = getNextValue();
  } else{
      result = NULL;
  }

  // Push into buffer
  addValue(std::move(_in));

  return std::move(result);
}

SamplePtr Buffer::pop(){
  SamplePtr _out;

  // If buffer is not empty, pop front SamplePtr element
  if(getSize() > 0){
    _out = getNextValue();
  }
  return std::move(_out);
}

void Buffer::clear(){
  pthread_mutex_lock(&_mutex);
  _buffer.clear();
  _count = 0;
  pthread_mutex_unlock(&_mutex);
}

Sample Buffer::getOldest(){
  Sample result;

  pthread_mutex_lock(&_mutex);

  SamplePtr extract;
  try{
    extract = std::move(_buffer.at(0));
  } catch (std::out_of_range& oor) {
    //Do nothing
  }
  
  if(extract.get() != NULL){
    //Perform deep copy
    result.setChannel(extract->getChannel());
    result.setValue(extract->getValue());
    result.setTime(extract->getTime());
    result.setDiag(extract->getDiag());
    result.setWarn(extract->getWarn());
    result.setName(extract->getName());  

    //Put back into buffer
    _buffer[0] = std::move(extract);
  }

  pthread_mutex_unlock(&_mutex);

  return result;
}