#include <FileLogger.hpp>
#include <ExceptionLogger.hpp>
#include <unistd.h>

FileLogger::FileLogger(const std::string& filename, bool isBinary /* = false */, bool override /* = false */){
    
    //Class defaults
    _file = NULL;
    _isBinary = isBinary;

    //Overrides previous content if specified
    if(override){
        remove(filename.c_str());
    }
    
    //Defines the file mode as 'append'
    std::string mode = "a";
    
    
    //If binary, add the binary modifier to file mode
    if(isBinary){
        mode+="b";
    }
    
    _file.reset(fopen(filename.c_str(),mode.c_str()));    
}

FileLogger::~FileLogger(){
  _file.reset();
}

bool FileLogger::write(const std::string& data){
    size_t bytesWritten = 0;
    if(!_isBinary && _file != NULL){
      bytesWritten = fwrite(data.c_str(),1,data.size(),_file.get());
    }
    
    return(bytesWritten == data.size());    
}

bool FileLogger::write(const std::vector<uint8_t>& bytes){
    size_t bytesWritten = 0;
    if(_isBinary && _file != NULL){
        bytesWritten = fwrite(bytes.data(),1,bytes.size(),_file.get());
    }
    
    return(bytesWritten == bytes.size());    
}
