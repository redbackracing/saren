#include <Saren.hpp>

#include <Redjack.hpp>
#include <Diagnostic.hpp>
#include <Converter.hpp>
#include <Warning.hpp>
#include <Server.hpp>
#include <FileWriter.hpp>

//The stream will look as follows
  // | RedJack | -> | Diagnostic | -> | Converter | -> | Warning | -> | Server | -> | FileWriter | -x
void Saren::constructStream(){
  FIFOPtr inputSamples(new FIFO());
  FIFOPtr diagSamples(new FIFO());
  FIFOPtr convSamples(new FIFO());
  FIFOPtr warnSamples(new FIFO());
  FIFOPtr servSamples(new FIFO());
  FIFOPtr writeSamples(new FIFO());

  _fifos.push_back(inputSamples);
  _fifos.push_back(diagSamples);
  _fifos.push_back(convSamples);
  _fifos.push_back(warnSamples);
  _fifos.push_back(servSamples);
  _fifos.push_back(writeSamples);

  FIFOPtr nullFIFO;

  ModulePtr Redjac(new Redjack("Redjack", nullFIFO, _fifos[0]));
  ModulePtr Diag(new Diagnostic("Diagnostic", _fifos[0], _fifos[1]));
  ModulePtr Convert(new Converter("Converter", _fifos[1], _fifos[2]));
  ModulePtr Warn(new Warning("Warning", _fifos[2], _fifos[3]));
  ModulePtr Serv(new Server("Server",_fifos[3],_fifos[4]));
  ModulePtr Writer(new FileWriter("FileWriter",_fifos[4],nullFIFO,false)); 

  _modules.push_back(Redjac);
  _modules.push_back(Diag);
  _modules.push_back(Convert);
  _modules.push_back(Warn);
  _modules.push_back(Serv);
  _modules.push_back(Writer);

}
