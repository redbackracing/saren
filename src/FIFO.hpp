#ifndef FIFO_HEADER
#define FIFO_HEADER

#include <Sample.hpp>
#include <pthread.h>
#include <vector>
#include <memory>

/* Asynchronous FIFO implementation for SamplePtrs
 * Can be extended for a general buffer implementation later
 * Should be used to communicate between Modules
 * Only use via FIFOPtr
 */
class FIFO {
public:
  FIFO(size_t capacity = 20);
  ~FIFO();

  /* To add values, use std::move(insert), as otherwise
   * the uniqueness of the unique_ptr is broken
   */
  void addValue(SamplePtr insert);

  /* No need to use std::move on output
   * Returns by value
   */  
  SamplePtr getNextValue();

  /* Capacity refers to overall size of buffer
   * Size refers to number of elements inside buffer
   */  
  size_t getCapacity();
  size_t getSize();

//Made protected so it can be used by child classes
protected:
  std::vector<SamplePtr> _buffer;
  pthread_mutex_t _mutex;
  pthread_cond_t _notEmpty, _notFull;
  size_t _size;
  size_t _count;
};

//Implemented as shared_ptr to use between multiple modules (ideally 2)
typedef std::shared_ptr<FIFO> FIFOPtr;

#endif
