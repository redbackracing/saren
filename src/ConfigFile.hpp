#ifndef CONFIG_FILE_HEADER
#define CONFIG_FILE_HEADER

#include <map>
#include <vector>

/****************************************************************************
 * This file acts as a repository for Configuration File structures
 * Take caution in modifying it, as it will be used by multiple bits of code
 * Refer to Config File Protocol in wiki for functional details of fields
 ****************************************************************************/

class ConfigFileOutput {
public:
	std::vector<int> live;
};

typedef std::pair<float,float> Range;
typedef std::pair<float,float> Point;

class ConfigFileInput {
public:
  std::string channelName;
  int channelNumber;
  float samplingRate;
  int readOrder;
  Range valueRange;
  Range signalRange;
  std::vector<Point> convSet;

};

//ConfigInputMap maps a channel number to its input structure
typedef std::map<int,ConfigFileInput> ConfigInputMap;

class ConfigFile {
public:
  ConfigInputMap InputMap;
  ConfigFileOutput Output;
};

#endif

