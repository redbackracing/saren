#include <ChannelBuffer.hpp>


ChannelBuffer::ChannelBuffer(const std::vector<int>& iChannels, size_t iCapacity, bool iAverage){
	_channels = iChannels;
  _average = iAverage;

  if(_average){
    constructAverageBuffers(iChannels,iCapacity);
  } else {
    constructBuffers(iChannels,iCapacity);
  }

}

SamplePtr ChannelBuffer::push(SamplePtr in){
  SamplePtr result;

  int channel = in->getChannel();

  if(validChannelCheck(channel)){
    if(_average){
      result = _averageBuffers[channel]->push(std::move(in));
    } else {
      result = _buffers[channel]->push(std::move(in));
    }
  } else {
    //Return the input if channel isn't valid
    result = std::move(in);
  }

  return std::move(result);
}

SamplePtr ChannelBuffer::pop(int channel){
  SamplePtr result;

  if(validChannelCheck(channel)){
    if(_average){
      result = _averageBuffers[channel]->pop();
    } else {
      result = _buffers[channel]->pop();
    }
  }

  return std::move(result);
}

bool ChannelBuffer::validChannel(int channel){
  bool result = false;

  for(size_t i = 0; i < _channels.size(); i++){
    if(channel == _channels[i]){
      result = true;
      break;
    }
  }

  return result;
}

void ChannelBuffer::snapShot(std::vector<Sample>& oSnapShot){
  
  for(size_t i = 0; i < _channels.size(); i++){
    int channel = _channels[i];
    Sample insert;
    if(_average){
      insert = _averageBuffers[channel]->getOldest();
    } else {
      insert = _buffers[channel]->getOldest();
    }

    oSnapShot.push_back(insert); 
  }  
  
}

SamplePtr ChannelBuffer::averageSample(int channel){
  SamplePtr result;

  if(validChannelCheck(channel) && _average){
    result = _averageBuffers[channel]->averageSample();
  }

  return std::move(result);
}

std::vector<int> ChannelBuffer::getChannels(){
  return _channels;
}

void ChannelBuffer::constructBuffers(const std::vector<int>& iChannels, size_t iCapacity){
  std::vector<int>::const_iterator iter;

  for(iter = iChannels.begin(); iter != iChannels.end(); ++iter){
    BufferPtr buf(new Buffer(iCapacity));
    _buffers.insert(std::make_pair((*iter),std::move(buf)));
  }
}

void ChannelBuffer::constructAverageBuffers(const std::vector<int>& iChannels, size_t iCapacity){
  std::vector<int>::const_iterator iter;

  for(iter = iChannels.begin(); iter != iChannels.end(); ++iter){
    AvgBufferPtr buf(new AverageBuffer(iCapacity));
    _averageBuffers.insert(std::make_pair((*iter),std::move(buf)));
  }
}

bool ChannelBuffer::validChannelCheck(int channel){
  bool result = false;

  if(validChannel(channel)){
    result = true;
  } else {
    std::stringstream ss;
    ss << "ChannelBuffer: Couldn't find channel " << channel;
    _exceptionLogger.writeLog(ss.str());
  }

  return result;
}