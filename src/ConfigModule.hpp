#ifndef CONFIG_MODULE_HEADER
#define CONFIG_MODULE_HEADER

#include <Module.hpp>
#include <ConfigFileParser.hpp>

class ConfigModule : public Module {
public:
  ConfigModule(const std::string& name, FIFOPtr input, FIFOPtr output);
  ~ConfigModule();

  /* Manages the current config file the module uses
   * Remember to use [start/stop] module after changing
   * Also note filename is only the file name itself, no "config/" or ".rbrc"
   */
  std::string getConfigFileName();
  void setConfigFileName(const std::string& iFilename);

  /* Overrides Module class startModule
   * Performs configuration file parsing before starting thread function
   * If parsing fails, returns false and doesn't initiate a thread
   */
  virtual bool startModule();

protected:
  /* Given a configuration file structure
   * Construct the relevant structures inside the particular module
   * Which use those structures
   */
  virtual void constructConfiguration(const ConfigFile& iCf) = 0;

  ConfigFileParser _cfp;
  std::string _configFileName;

};

/* Provides a shared pointer interface for ConfigModule
 * Can be cast from a ModulePtr to use ConfigModule specific functions
 */
typedef std::shared_ptr<ConfigModule> ConfigModulePtr;

#endif