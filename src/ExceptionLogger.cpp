#include <ExceptionLogger.hpp>
#include <sstream>

ExceptionLogger::ExceptionLogger() {
  _logger.reset(new FileLogger("exception.log"));
  _logger->write("Starting exceptionLogger");
  _logger->write("\n");
  pthread_mutex_init(&_mutex,NULL);    
}

ExceptionLogger::~ExceptionLogger(){
  _logger->write("Stopping exceptionLogger");
  _logger->write("\n");
  _logger.reset();
  pthread_mutex_destroy(&_mutex);
}

void ExceptionLogger::write(const std::string& logLine){ 
  pthread_mutex_lock(&_mutex);
  
  _logger->write(logLine);
  _logger->write("\n");
    
  pthread_mutex_unlock(&_mutex);
}

void ExceptionLogger::writeLog(const std::string& logLine){
  std::stringstream ss;
  ss << logLine;
	ExceptionLogger& logger = ExceptionLogger::getInstance();
	logger.write(ss.str());
}

void ExceptionLogger::writeLog(const char* logLine){
  if(logLine != NULL){
    const std::string ll(logLine);
    std::stringstream ss;
    ss << logLine;
    ExceptionLogger& logger = ExceptionLogger::getInstance();
    logger.write(ss.str());    
  }
}

/* In C++11, the Double Check Locking Procedure isn't required
 * as the language is only allowed to perform object initialisation
 * on 1 thread
 */
ExceptionLogger& ExceptionLogger::getInstance(){
  static ExceptionLogger _instance;
  return _instance;
}
