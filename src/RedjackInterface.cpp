#include <RedjackInterface.hpp>
#include <ExceptionLogger.hpp>
#include <exception>
#include <string>
#include <iostream>

using namespace std;

/**************************************** COMMS Functions **************************************/
RedjackInterface::RedjackInterface(std::vector<int> _channels, float _scanRate, ChannelBufferPtr _buffer){
	channels = _channels;
	scanRate = _scanRate;
	buffer   = _buffer;

	// initialises the USB connection
	openLabjack();
}

RedjackInterface::~RedjackInterface(){
}

void RedjackInterface::openLabjack(){

  // If device is still open, NULL is returned
	hDevice = LJUSB_OpenDevice(1,0,U6_PRODUCT_ID);
	if( hDevice == NULL ){
		_exceptionLogger.writeLog("Error: Labjack cannot be opened. Please check connection or close device and try again.");
		return;
	}
	// Stops any previously running streams when connection is opened - will throw exception if it was stopped but exception won't stop function
	StreamStop();
}

void RedjackInterface::closeLabjack(){

	// Closes USB connection ot the device
	LJUSB_CloseDevice(hDevice);
}

bool RedjackInterface::isDeviceConnected(){
	
	// Checks if device is connected
	if( LJUSB_IsHandleValid(hDevice) == true ){
		return true;
	}
	else {
		_exceptionLogger.writeLog("Error: Labjack connection is not open. Please open device connection again.");
		return false;
  }
}

/*********************************** Calibration functions *************************************/

// Gets default calibration info from hardware flash memory blocks 0-10 of U6 and stores it in
// calibration structure defined
void RedjackInterface::getCalibrationInfo(){
	exception error;
	BYTE sendBuff[64], recBuff[64];
	int offset = 0, i;

	// Reading calibration info within hardware flash
	for( i=0; i<10; i++ ){
		sendBuff[1] = (BYTE)(0xf8);
		sendBuff[2] = (BYTE)(0x01);
		sendBuff[3] = (BYTE)(0x2d);
		sendBuff[6] = 0;
		sendBuff[7] = (BYTE)i; // Indentifies which memory block number to look into
		extendedChecksum( sendBuff, 8 );

		// checks for potential errors with communication
		try{
			if( LJUSB_Write( hDevice, sendBuff, 8 ) < 8 ){
				throw error;
			}
			if( LJUSB_Read( hDevice, recBuff, 40 ) < 40 ){
				throw error;
			}
		}
		catch( exception& e){
			_exceptionLogger.writeLog("Error: Communication between Labjack failed during getCalibrationInfo call.");
			return;
		}
		try{
			if( !(verifyChecksum( recBuff, 40, "getCalibrationInfo"))){
				throw error;
			}
			if( recBuff[1] != (BYTE)(0xf8) || recBuff[2] != (BYTE)(0x11) || recBuff[3] != (BYTE)(0x2d) ){
				_exceptionLogger.writeLog("Error: Wrong command bytes recieved for ReadMem in getCalibrationInfo call.");
				throw error;
			}
			if( recBuff[6] != 0 ){
				_exceptionLogger.writeLog("Error in getCalibrationInfo. ErrorCode = " + to_string(recBuff[6]));
				throw error;
			}
		}
		catch( exception& e ){
			return;
		}
		
		offset = i*4; // Each block has 4 nominal values associated with it - see calibration info struct
		// Updating calibration struct, starting by Byte 8 of recBuff, with info from U6 itself
		caliInfo.Constants[offset]     = FPByteArrayToFPDouble(recBuff + 8, 0);
		caliInfo.Constants[offset + 1] = FPByteArrayToFPDouble(recBuff + 8, 8);
		caliInfo.Constants[offset + 2] = FPByteArrayToFPDouble(recBuff + 8, 16);
		caliInfo.Constants[offset + 3] = FPByteArrayToFPDouble(recBuff + 8, 24);
	}
	caliInfo.prodID = 6; // Product ID to identify U6
	
	return;
}

// Function to convert binary voltages in stream packet into analog voltages through calibration constants
void RedjackInterface::getBinaryToVolt(int resolutionIndex, int gainIndex, int bits24, unsigned short voltageBytes, float *analogVolt){
	float value = 0;
	int indexAdjust = 0;

	/*
	* TODO: Check for validation of calibration constants and struct
	*/
	
	// checks if arguments are not out of bounds
	value = (float)voltageBytes;
	if( bits24 ){
        value = value/256.0;
	}
  if( resolutionIndex > 8 ){
      indexAdjust = 24;
	}
  if( gainIndex > 4 )
  {
      _exceptionLogger.writeLog("Error: gain index out-of-bounds in getBinaryToVolt call");
      return;
  }

	// Calibration adjustments on the raw voltage values being passed in
  if( value < caliInfo.Constants[indexAdjust + gainIndex*2 + 9] ){
      *analogVolt = (caliInfo.Constants[indexAdjust + gainIndex*2 + 9] - value) * caliInfo.Constants[indexAdjust + gainIndex*2 + 8];
	}
  else{
      *analogVolt = (value - caliInfo.Constants[indexAdjust + gainIndex*2 + 9]) * caliInfo.Constants[indexAdjust + gainIndex*2];
	}
	return;
} 

// Fixed-point Byte array to fixed-point double array function
double RedjackInterface::FPByteArrayToFPDouble(unsigned char *buffer, int startIndex){
	BYTES4 resultDec = 0, resultWh = 0;

	resultDec = (BYTES4)buffer[startIndex] |
		          ((BYTES4)buffer[startIndex + 1] << 8) |
		          ((BYTES4)buffer[startIndex + 2] << 16) |
		          ((BYTES4)buffer[startIndex + 3] << 24);

	resultWh = (BYTES4)buffer[startIndex + 4] |
		          ((BYTES4)buffer[startIndex + 5] << 8) |
		          ((BYTES4)buffer[startIndex + 6] << 16) |
		          ((BYTES4)buffer[startIndex + 7] << 24);

	return ( (double)((int)resultWh) + (double)(resultDec)/4294967296.0 );
}

// Checks whether the stored calibration constants are valid or not
/*
*
* TODO: Need to make a validation function on Calibration constants
*/

/******************************************** STREAM functions *********************************************/

// Stream Configuration - clock, channel and samples per packet configuration
void RedjackInterface::StreamConfig(){

	// samples in a packet is the same as number of channels - each sample correlates to each channel
	const BYTE samplesPerPacket = (BYTE)channels.size();
	int sendBuffSize = 14+samplesPerPacket*2;
	BYTE sendBuff[sendBuffSize], recBuff[8];
	int i;
	exception error;

	sendBuff[1] = (BYTE)(0xF8);
	sendBuff[2] = 4 + channels.size();
	sendBuff[3] = (BYTE)(0x11);
	sendBuff[6] = channels.size();
	sendBuff[7] = 1; 
	sendBuff[8] = samplesPerPacket;
	sendBuff[9] = 0;
	sendBuff[10] = 0;
	sendBuff[11] = 2; // Bytes used to configure the stream scan rate - little endian - assumes 4MHz/256 as base clock rate
	sendBuff[12] = (BYTE)((int)(scanRate*baseClk)&(0x00FF)); // scan interval - low byte
	sendBuff[13] = (BYTE)((int)(scanRate*baseClk)/256); // scan interval - high byte
	// Adding channel list to sample within the stream table
	for( i=0; i<channels.size(); i++ ){
		sendBuff[14 + i*2] = channels.at(i);
		sendBuff[15 + i*2] = 0;
	}
	// adds checksum values to checksum elements
	extendedChecksum(sendBuff, sendBuffSize);

	// checks for potential errors with comms
	try{
		if( LJUSB_Write( hDevice, sendBuff, sendBuffSize ) < sendBuffSize ){
			throw error;
		}
		for( i=0; i<8; i++ ){
			recBuff[i] = 0;
		}
		if( LJUSB_Read( hDevice, recBuff, 8 ) < 8 ){
			throw error;
		}
	}
	catch( exception& e){
		_exceptionLogger.writeLog("Error: Communication between Labjack failed during StreamConfig call.");
		return;
	}

	try{
		if( !(verifyChecksum( recBuff, 8, "StreamConfig"))){
			throw error;
		}
		if( recBuff[1] != (BYTE)(0xf8) || recBuff[2] != (BYTE)(0x01) || recBuff[3] != (BYTE)(0x11) || recBuff[7] != (BYTE)(0x00) ){
			_exceptionLogger.writeLog("Error: Wrong command bytes recieved for Stream configuration in StreamConfig call.");
			throw error;
		}
		if( recBuff[6] != 0 ){
			_exceptionLogger.writeLog("Error in StreamConfig. ErrorCode = " + to_string((int)recBuff[6]));
      throw error;
		}
	}
	catch( exception& e ){
		return;
	}
	return;
}

// Starting streaming and opening pipe for stream
void RedjackInterface::StreamStart(){

	BYTE sendBuff[2], recBuff[4];
	exception error;

	sendBuff[0] = (BYTE)(0xa8);
	sendBuff[1] = (BYTE)(0xa8);
	
	try{	
		if( (LJUSB_Write(hDevice, sendBuff, 2)) < 2 ){
			throw error;
		}
		if( (LJUSB_Read(hDevice, recBuff, 4)) < 4 ){
			throw error;
		} 
	}
	catch( exception& e){
		_exceptionLogger.writeLog("Error: Communication between Labjack failed during StreamStart call.");
		return;
	}

	try{
		if( Checksum(recBuff, 4) != recBuff[0] ){
			_exceptionLogger.writeLog("Error: Checksum in byte 0 did not match in StreamStart.");
			throw error;
		}
		if( recBuff[1] != (BYTE)(0xa9) || recBuff[3] != (BYTE)(0x00) ){
			_exceptionLogger.writeLog("Error: Wrong command bytes recieved for Stream start in StreamStart call.");
			throw error;
		}
	}
	catch( exception& e ){
		return;
	}
	if( recBuff[2] != 0 ){
			_exceptionLogger.writeLog("Error in StreamStart. ErrorCode = "+ to_string(recBuff[2]));

	}
	return;
}

// Stopping streaming and closing pipe
void RedjackInterface::StreamStop(){

	BYTE sendBuff[2], recBuff[4];
	exception error;

	sendBuff[0] = (BYTE)(0xb0);
	sendBuff[1] = (BYTE)(0xb0);
	
	try{	
		if( (LJUSB_Write(hDevice, sendBuff, 2)) < 2 ){
			throw error;
		}
		if( (LJUSB_Read(hDevice, recBuff, 4)) < 4 ){
			throw error;
		} 
	}
	catch( exception& e){
		_exceptionLogger.writeLog("Error: Communication between Labjack failed during StreamStop call.");
		return;
	}

	try{
		if( Checksum(recBuff, 4) != recBuff[0] ){
			_exceptionLogger.writeLog("Error: Checksum in byte 0 did not match in StreamStop.");
			throw error;
		}
		if( recBuff[1] != (BYTE)(0xb1) || recBuff[3] != (BYTE)(0x00) ){
			_exceptionLogger.writeLog("Error: Wrong command bytes recieved for Stream stop in StreamStop call.");
			throw error;
		}
	}
	catch( exception& e ){
		return;
	}
	if( recBuff[2] != 0 ){
		_exceptionLogger.writeLog("Error in StreamStop. ErrorCode = " + to_string(recBuff[2])) ;
	}
	return;
}

// Retrieves scans of channels
// getStream returns number of byte samples in the hardware back log
int RedjackInterface::getStream(int *packetCounter){

	exception error;
	int k, channelIndex, backLog, responseSize, dataSize, transmittedBytes;
	BYTES2 voltBytes;
	float voltageVal;
	
	// responseSize is the size of the packet stream gets back with
	responseSize = 14 + channels.size()*2;
	// dataSize is the element number at which the backlog information can be found within the packet
	dataSize = 12 + channels.size()*2;
	BYTE recBuff[responseSize];
	
	// Checks whether the returned number of bytes is the excepted number of bytes
	if( (transmittedBytes = LJUSB_Stream(hDevice, recBuff, responseSize)) < responseSize ){
    _exceptionLogger.writeLog("Error: Stream failed. Bytes retrieved  = " + to_string(transmittedBytes));
		(*packetCounter)++;
		// returns without processing if stream isn't successful to return a packet
		return -1;
	}

	backLog = (int)recBuff[dataSize];

	// Grabs individual samples from the stream packet and processes the voltage value by checking to see if it was an analog to digital channel
	// Each sample is 2 bytes
  for( k = 12, channelIndex = 0; k < (dataSize), channelIndex < channels.size(); k += 2, channelIndex++){
		voltBytes = (BYTES2)recBuff[k] + (BYTES2)recBuff[k+1]*256;
		if( channels[channelIndex] < 14 ){ // less than analog limit
			getBinaryToVolt(1, 0, 0, voltBytes, &voltageVal); 		
		}
		else{
			voltageVal = 0;
		/*
		*	 TODO: Implement digital channel input check and handling
		*/
		}
		// Adding new constructed sample into the ChannelBuffer object
		// Checks for valid  channel buffer within the ChannelBuffer instance
		if( buffer->validChannel(channels[channelIndex]) ){
			SamplePtr in( new Sample((channels[channelIndex]), voltageVal, (*packetCounter)*scanRate) );
      SamplePtr poppedSample = buffer->push( std::move(in) );
			// if the buffer is full, the oldest sample is removed from buffer and returned
			if( poppedSample != NULL ){
				poppedSample.reset(); // sample is freed
				poppedSample = NULL;
			}	
		}
	}
	// variable is incremented to convey correct sampling time for each scan
	++(*packetCounter);

	return backLog;
}


/**************************************************************************************
* Checksum Methods - good for validating response/send buffers for comms.
* Unique checksum methods used on response/send buffers 
*                    of different sizes.
***************************************************************************************/

// Checksum for normal command using 2^8 as divisor - returns 1 byte checksum
// NOTE: If used on extended command, second argument should be 6
unsigned char RedjackInterface::Checksum(unsigned char *b, int n){
	int i;
	BYTES2 a, bb;

	for( i=1, a=0; i<n; i++ ){
		a += (BYTES2)b[i];
	}

	bb = a/256;
	a  = (a-256*bb)+bb;
	bb = a/256;

	return (BYTE)((a-256*bb)+bb);
}

// Adding all bytes from 6 to n-1 and returning a 2 byte sum - used for extended commands
unsigned short RedjackInterface::totalSum(unsigned char *b, int n){
	int i, a=0;

	for( i=6; i<n; i++){
		a += (BYTES2)b[i];
	}

	return a;
}

// Wrapping checksum method for any extended commands
void RedjackInterface::extendedChecksum(unsigned char *b, int n){
	BYTES2 a;

	a = totalSum(b,n);
	b[4] = (BYTE)(a & 0xff);
	b[5] = (BYTE)((a/256) & 0xff);
	b[0] = Checksum(b,6);
}

// Verifies checksum in extended command packets for various functions
bool RedjackInterface::verifyChecksum(unsigned char *response, int numBytes, string funcName){
	BYTES2 checksumTotal = 0;
	bool _isChecksumEqual = true;

 	checksumTotal = totalSum(response, numBytes);

  if( (BYTE)((checksumTotal / 256) & 0xff) != response[5] ){
		_isChecksumEqual = false;
		_exceptionLogger.writeLog("Error: Checksum error with Byte 5 in " + funcName); 
	}

  if( (BYTE)(checksumTotal & 0xff) != response[4] ){
		_isChecksumEqual = false;		
		_exceptionLogger.writeLog("Error: Checksum error with Byte 4 in " + funcName); 
	}

  if( Checksum(response,6) != response[0] ){
		_isChecksumEqual = false;  	
		_exceptionLogger.writeLog("Error: Checksum error with Byte 0 in " + funcName); 
	}

	return _isChecksumEqual;
}
