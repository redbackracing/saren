#include <Redjack.hpp>
#include <iostream>
#include <sstream>
#include <exception>

Redjack::Redjack(const std::string& name, FIFOPtr input, FIFOPtr output, int _capacity /* = 20 */) : ConfigModule(name,input,output){
  bufferCapacity = _capacity;
}

Redjack::~Redjack(){
}

SamplePtr Redjack::internalProcess(){
  SamplePtr _sample = NULL;

  for(std::vector<int>::iterator it=channels.begin(); it != channels.end(); ++it){
    if(channelBuffers->validChannel(*it)){
      _sample = channelBuffers->pop(*it);
    }
  }

  return std::move(_sample);
}

void Redjack::constructConfiguration(const ConfigFile& _container){
  ConfigInputMap _inputMap(_container.InputMap);

  /* Push channel - sampling rate pair values and channels into respective
  * continers
  */
  ConfigInputMap::iterator mapIter;
  for(mapIter = _inputMap.begin(); mapIter != _inputMap.end(); ++mapIter){
    if(mapIter->second.samplingRate > 0){
      samplingRateMap.insert(std::make_pair(mapIter->first, mapIter->second.samplingRate));
      channels.push_back(mapIter->first);
    }
  }

  // Creates buffer of channel buffers for Samples
  channelBuffers.reset((new ChannelBuffer(channels, bufferCapacity, false)));

  // Gets highest sampling rate from sampling rate map
  float _scanRate = getMaxRate();

  // Creates Redjack Interface
  RedjackIface.reset((new RedjackInterface(channels, (1/_scanRate), channelBuffers)));

  return;
}

bool Redjack::startModule(){
  bool _create = false;

  // Creates and starts internal interface thread
  if(ConfigModule::startModule()){  
    if(pthread_create(&ifaceThread, NULL, InterfaceThreadFunction, this) == 0){
      _create   = true;
      operation = true;
    } else {
      std::stringstream ss;
      ss << "[REDJACK]: Unable to create interface thread...";
      _exceptionLogger.writeLog(ss.str()); 
    }
  }
  return _create;
}

bool Redjack::stopModule(){
  bool _stop = false;
  operation = false;
  // Stops Labjack streaming and closes connection
  RedjackIface->StreamStop();
  RedjackIface->closeLabjack();

  //std::cout << "Interface streaming and pipe closed." << std::endl;
  //Module::setRunning(false);
  //std::cout << "[REDJACK]: Stopping Redjack threads..." << std::endl;
  // Exits thread
  int _join = pthread_join(ifaceThread, NULL);
  if((_join == 0) && (Module::stopModule())){
    _stop = true;
  } else {
    _exceptionLogger.writeLog("[REDJACK]: Join failed!");
  }

  return _stop;
}

void* Redjack::InterfaceThreadFunction(void* _this){
  Redjack* _redjack = (Redjack*)_this;
  int _backlog;
  int _packetCounter = 0;

  // Checks if device is infact connected
  if(!(_redjack->RedjackIface->isDeviceConnected())){
    return NULL;
  }

  // Sets up Labjack for streaming
  _redjack->RedjackIface->getCalibrationInfo();
  _redjack->RedjackIface->StreamConfig();
  _redjack->RedjackIface->StreamStart();

  while(_redjack->operation){
    //std::cout << "Running..." << std::endl;
    if( _redjack->RedjackIface->isDeviceConnected()){
      //std::cout << "logging data..." << std::endl;
      _backlog = _redjack->RedjackIface->getStream(&_packetCounter);
      // Hardware buffer is roughly 25% full at this point -> 255 is 100% full
      if(_backlog > 57){
        std::stringstream ss;
        ss << "[REDJACK]: HW buffer is " << _backlog << " full. Please lower maximum sampling rate.";
        _redjack->_exceptionLogger.writeLog(ss.str());
      } 
    }
  }
  //std::cout << "REDJACK is not running" << std::endl;
  return NULL;
}

float Redjack::getMaxRate(){
  channelRatePair _max = *std::max_element(samplingRateMap.begin(), samplingRateMap.end(), compareRates());
  return _max.second;
}

std::vector<int> Redjack::getChannelVect(){
  return channels;
}

std::map<int, float> Redjack::getSamplingRateMap(){
  return samplingRateMap;
}
