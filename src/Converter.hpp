#ifndef CONVERTER_HEADER
#define CONVERTER_HEADER

#include <ConfigModule.hpp>
#include <map>

#define ANALOG_MIN 1
#define ANALOG_MAX 14

#define DIGITAL_MIN 15
#define DIGITAL_MAX 35

class Converter : public ConfigModule {
public:
  Converter(const std::string& name, FIFOPtr input, FIFOPtr output);
  ~Converter();

	/* Overriden module-specific process function which 
	 * ultilises either analogProcess, digitalProcess and/or 
	 * CANProcess to correctly process the current sample 
	 */
  virtual SamplePtr process(SamplePtr _current);

protected:
  virtual void constructConfiguration(const ConfigFile& _container);

private:
  std::map<int,std::vector<Point>> conversions;

  SamplePtr analogProcess(SamplePtr _current);
  SamplePtr digitalProcess(SamplePtr _current);
  SamplePtr CANProcess(SamplePtr _current);	
};

#endif
