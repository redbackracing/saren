#ifndef REDJACK
#define REDJACK

#include <ChannelBuffer.hpp>
#include <RedjackInterface.hpp>
#include <ConfigModule.hpp>

#include <pthread.h>
#include <map>
#include <algorithm>

typedef std::pair<int, int> channelRatePair;

class Redjack : public ConfigModule {
public: 
  Redjack(const std::string& name, FIFOPtr input, FIFOPtr output, int _capacity = 20);
  ~Redjack();

  /* The interface will package samples into SamplePtrs
   * and place them into a buffer, categorised by their
   * channel numbers. InternalProcess accounts for this
   * processing and outputs those SamplePtrs.
   */
  virtual SamplePtr internalProcess();

  /* Internal thread control function
   * Start and stop interface through these functions
   */
  virtual bool startModule();
  virtual bool stopModule();

  std::vector<int> getChannelVect();
  std::map<int, float> getSamplingRateMap(); 

  /* Retrieves maximum sampling rate from the map
   * to set labjack scaning rate
   */
  float getMaxRate();

protected:
  virtual void constructConfiguration(const ConfigFile& _container);

private:
  ChannelBufferPtr channelBuffers = NULL;
  RedjackIfacePtr  RedjackIface   = NULL;
  int bufferCapacity;
  pthread_t ifaceThread;
  bool operation = false; 
  /* Sampling rate map to keep a record
   * of channel numbers and their sampling
   * rates. This will be used for identifying
   * upperbound sampling rate for the interface
   */
  std::map<int, float> samplingRateMap;
  std::vector<int> channels;
  
  /* Interface thread functions to start the labjack
   * processing on a seperate thread
   */
  static void* InterfaceThreadFunction(void* _this);

  /* Method for comparing pairs in the sampling rate map
   * Used in the method to find the largest sampling rate for
   * Labjack stream configuration
   */
  struct compareRates{
    bool operator()(const channelRatePair& _left, const channelRatePair& _right){
      return _left.second < _right.second;
    }
  };
};

typedef std::shared_ptr<Redjack> RedjackPtr;

#endif
