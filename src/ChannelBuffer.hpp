#ifndef CHANNEL_BUFFER_HEADER
#define CHANNEL_BUFFER_HEADER

#include <Buffer.hpp>
#include <AverageBuffer.hpp>
#include <Sample.hpp>
#include <ExceptionLogger.hpp>

#include <map>
#include <vector>

/* Class which implements channel-specific buffers
 * To use as a temporary window within Modules to perform operations
 * such as computing average samples or to hold a 'snapshot' of samples for telemetry
 */
class ChannelBuffer {
public:
  /* Constructs a ChannelBuffer based on:
   * iChannels - Vector of channel numbers to be made
   * iCapacity - The capacity of each buffer inside ChannelBuffer, must be uniform
   * iAverage - If the buffers inside should be AverageBuffers or not
   */
  ChannelBuffer(const std::vector<int>& iChannels, size_t iCapacity, bool iAverage = false);

  /* Push a samplePtr object into the channel's buffer.
   * If buffer is full, a SamplePtr object is popped
   * Writes exception if it cannot find channel
   * Returns NULL samplePtr if buffer is not full or cannot find channel
   */
  SamplePtr push(SamplePtr in);

  /* Pops a SamplePtr from the front of channel buffer
   * and moves it to current scope.
   * Returns NULL samplePtr if it cannot find the channel
   * or if channel buffer is empty
   */
  SamplePtr pop(int channel);

  /* Checks if channel in argument has an associated buffer
   * in ChannelBuffer
   */
  bool validChannel(int channel);

  /* Returns a snap shot of the oldest Samples inside 
   * channel buffer
   * Performs a deep copy
   */ 
  void snapShot(std::vector<Sample>& oSnapShot);

  /* Given a particular channel
   * Returns the average sample over it's buffer
   * And clears the buffer
   * Returns NULL samplePtr if channel not found
   * or buffer is completely empty
   */
  SamplePtr averageSample(int channel);

  
  std::vector<int> getChannels();

private:

  void constructBuffers(const std::vector<int>& iChannels, size_t iCapacity);
  void constructAverageBuffers(const std::vector<int>& iChannels, size_t iCapacity);

  bool validChannelCheck(int channel);
  
  std::vector<int> _channels;

  //If _average is false, this is used
  std::map<int,BufferPtr> _buffers;

  //If _average is true, this is used
  std::map<int,AvgBufferPtr> _averageBuffers;

  bool _average;

  ExceptionLogger& _exceptionLogger = ExceptionLogger::getInstance();
};

typedef std::shared_ptr<ChannelBuffer> ChannelBufferPtr;

#endif
