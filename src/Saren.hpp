#ifndef SAREN_HEADER
#define SAREN_HEADER

#include <ModuleStream.hpp>

class Saren : public ModuleStream {
public:
  virtual void constructStream();
};

#endif