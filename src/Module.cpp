#include <Module.hpp>

Module::Module(const std::string& name, FIFOPtr input, FIFOPtr output){

  pthread_mutex_init(&_runningLock,NULL);
  _name = name;
  _running = false;
  _input = input;
  _output = output;

}

Module::~Module(){

  pthread_mutex_destroy(&_runningLock);

}

//Creates thread based on contents of threadFunc
bool Module::startModule(){
  setRunning(true);
  return (pthread_create(&_thread,NULL,threadFunc,this) == 0);
}

//Sets flag of module to stop and waits for exit
bool Module::stopModule(){
  setRunning(false);
  return (pthread_join(_thread,NULL) == 0);
}

//Returns if module is running
bool Module::getRunning(){
  bool result = false;

  pthread_mutex_lock(&_runningLock);

  result = _running;

  pthread_mutex_unlock(&_runningLock);

  return result;
}

//Sets the running state of Module
void Module::setRunning(bool state){
  pthread_mutex_lock(&_runningLock);

  _running = state;

  pthread_mutex_unlock(&_runningLock);
}

void* Module::threadFunc(void* thisModule){
  Module* ptr = (Module*)thisModule;
  ptr->stateMachine();
  return NULL;
}

void Module::stateMachine(){
  
  //Utilise local SamplePtr container
  SamplePtr current;

  //Check module is running at each iteration, otherwise exit
  while(getRunning()){
    //Check input fifo is valid and has elements
    if(_input.get() != NULL && _input->getSize() > 0){
      
      //Gather next SamplePtr from FIFO and process      
      current = _input->getNextValue();
      current = process(std::move(current));

    } else {
    //Otherwise run internalProcess
      current = internalProcess();
    }

    //If the output of process is valid
    if(current.get() != NULL){

      //Check output fifo is valid
      if(_output.get() != NULL){
        //Send to output fifo
        _output->addValue(std::move(current));
      } else {
        //Delete pointer
        current.reset();
      }
    }
  }
}

//Default implementation, can be overridden
SamplePtr Module::process(SamplePtr insert){
  return std::move(insert);
}

//Default implementation, can be overridden
SamplePtr Module::internalProcess(){
  SamplePtr output = NULL;
  return std::move(output);
}

std::string Module::getName(){
  return _name;
}
