#include <Server.hpp>
#include <unistd.h>

std::string Server::hostIP = "54.206.75.71";
unsigned short Server::port = 12001;

Server::Server(const std::string& name, FIFOPtr input, FIFOPtr output):ConfigModule(name,input,output) {
  _sockfd = -1;
  _connected = false;

  //Set initial time
  _lastSentPush = steady_clock::now();
}

void Server::constructConfiguration(const ConfigFile& _container){
  _channels = _container.Output.live;

  // Clears current names map  
  _names.clear();

  // Map variable relating channel number to the input structure
  ConfigInputMap aInputMap = _container.InputMap;
  
  // Iterates through map of inputs to map channel number to conversion vector
  ConfigInputMap::iterator mapIter;
  for(mapIter = aInputMap.begin(); mapIter != aInputMap.end(); ++mapIter){
    if(!mapIter->second.channelName.empty()){
      _names.insert(std::make_pair(mapIter->first,mapIter->second.channelName));
    }
  }

  _buffer.reset(new ChannelBuffer(_channels,1,false));

  tryConnect();
}

SamplePtr Server::process(SamplePtr _current){
  //Add name
  _current->setName(_names[_current->getChannel()]);

  //Push to buffer
  _current = _buffer->push(std::move(_current));

  int timeDiff = duration_cast<milliseconds>(steady_clock::now() - _lastSentPush).count();

  if(timeDiff >= 50){ //If timediff more than 50 ms
    //Construct blob
    std::vector<Sample> blobContent;
    _buffer->snapShot(blobContent);

    std::string blob = _compressor.compress(blobContent);

    //Send blob to server
    if(_sockfd >= 0 || !_connected){
      tryConnect();

      // append the time sent to the blob
      auto millisecondsSinceEpoch =
        std::chrono::duration_cast<std::chrono::milliseconds>
          (std::chrono::system_clock::now().time_since_epoch()).count();

      blob.append(" Sent: " + std::to_string(millisecondsSinceEpoch));

      int numBytesWritten = write(_sockfd,blob.data(),blob.size());
      if(numBytesWritten < 0){
        _exceptionLogger.writeLog("Server: Failed to write to socket");
        _connected = false;
      }
    } else {
      tryConnect();
    }
    
    _lastSentPush = steady_clock::now();
  }

  return std::move(_current);
}

void Server::tryConnect(){
  //Create and bind UDP socket for sending
  _sockfd = socket(AF_INET,SOCK_DGRAM,0);
  
  if(_sockfd < 0){
    _exceptionLogger.writeLog("Server: Couldn't create socket");
  }

  _serv_addr.sin_family = AF_INET;
  _serv_addr.sin_addr.s_addr = inet_addr(hostIP.c_str());
  _serv_addr.sin_port = htons(port);

  if(connect(_sockfd,(struct sockaddr *) &_serv_addr,sizeof(_serv_addr)) < 0){
    _exceptionLogger.writeLog("Server: Couldn't associate AWS server address with socket");
  } else {
    _connected = true;
  }
}
