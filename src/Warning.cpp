#include <Warning.hpp>
#include <sstream>
#include <ExceptionLogger.hpp>

using namespace std;

Warning::Warning(const std::string& name, FIFOPtr input, FIFOPtr output):ConfigModule(name,input,output){

}

Warning::~Warning(){

}

//reads config file and creates map of ranges
void Warning::constructConfiguration(const ConfigFile& _container){
  // Clears current conversion map  
  ranges.clear();

  // Map variable relating channel number to the input structure
  ConfigInputMap _inputMap(_container.InputMap);

  // Iterates through map of inputs to map channel number to ranges
  ConfigInputMap::iterator mapIter;
  for(mapIter = _inputMap.begin(); mapIter != _inputMap.end(); ++mapIter){
      ranges.insert(std::make_pair(mapIter->first,mapIter->second.valueRange));
  }
  return;
}

SamplePtr Warning::process(SamplePtr p){

  //Read in channel number, set Diag flag to false
  uint32_t chn = p->getChannel();
  int chn1 = (int)chn;
  p->setWarn(false);

  typedef std::map<int,Range>::iterator iter;
  bool inRange = false;
  iter iter1;

  //Iterate through ranges map; determine if sample channel is in map
  for(iter1 = ranges.begin(); (iter1 != ranges.end()) && (inRange == false); ){
    if(iter1->first == chn1){
      inRange = true;
    }
    else{
      iter1 ++;
    }
  }

//If the channel is found, compare
  if(inRange == true){
    if(p->getValue() < iter1->second.first || p->getValue() > iter1->second.second){
      p->setWarn(true);
    }
  }

//If the channel is not found, write to exception.log
  if(inRange == false){
    ExceptionLogger& _exceptionLogger = ExceptionLogger::getInstance();
    std::stringstream ss;
	ss << "Channel " << p->getChannel() << " not found";
	_exceptionLogger.writeLog(ss.str());
  }

//Return the sample, with the now appropriate warn flag
  return std::move(p);
}