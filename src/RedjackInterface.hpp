/***********************************************************
*
*	RedjackInterface 
*
*	Labjack Library interface, taking care of Library function 
* details so that developers do no have to work with them
*
*	Updated: 29/11/2016
*
************************************************************/

#ifndef REDJACK_IFACE_HEADER
#define REDJACK_IFACE_HEADER

#include <labjackusb.h>
#include <ChannelBuffer.hpp>
#include <ExceptionLogger.hpp>

#include <cmath>
#include <string>
#include <stdlib.h>

typedef unsigned short BYTES2;
typedef unsigned int   BYTES4;

// Default calibration constant just for reference
typedef struct U6_CALIBRATION_INFO{
	BYTE prodID = 6;
	double Constants[40] = {
		0.00031580578,
    -10.5869565220,
    0.000031580578,
    -1.05869565220,
    0.0000031580578,
    -0.105869565220,
    0.00000031580578,
    -0.0105869565220,
    -.000315805800,
    33523.0,
    -.0000315805800,
    33523.0,
    -.00000315805800,
    33523.0,
    -.000000315805800,
    33523.0,
    13200.0,
    0.0,
    13200.0,
    0.0,
    0.00001,
    0.0002,
    -92.379,
    465.129,
    0.00031580578,
    -10.5869565220,
    0.000031580578,
    -1.05869565220,
    0.0000031580578,
    -0.105869565220,
    0.00000031580578,
    -0.0105869565220,
    -.000315805800,
    33523.0,
    -.0000315805800,
    33523.0,
    -.00000315805800,
    33523.0,
    -.000000315805800,
    33523.0};
} u6CalibrationInfo;

class RedjackInterface{
private:
	HANDLE            hDevice = 0;
	u6CalibrationInfo caliInfo;
	std::vector<int>  channels;                 // Vector of channels
	float             scanRate;                 // Scanning rate in seconds
	
	// baseClk is defined to be 4MHz/256 as chosen by the scanConfig bytes in function 'StreamConfig()'
	// It has been pre-defined to calculate scanning rate for the Labjack (see StreamConfig())
	float baseClk            = 15625;             // Default base clock in Hz - hard-coded
	ChannelBufferPtr buffer  = NULL;              // ChannelBuffer for populating

	/** Internal functions for Calibration functions **/
	void getBinaryToVolt(int _resolutionIndex, int _gainIndex, int _bits24, unsigned short _voltageBytes, float *_analogVolt);
	double FPByteArrayToFPDouble(unsigned char *_buffer, int _startIndex);

	/** Checksum functions **/
	unsigned char Checksum(unsigned char *_b, int _n);
	unsigned short totalSum(unsigned char *_b, int _n);
	void extendedChecksum(unsigned char *_b, int _n);
	bool verifyChecksum(unsigned char *_reponse, int _numBytes, std::string _funcName);

  ExceptionLogger& _exceptionLogger =  ExceptionLogger::getInstance();

public:
	RedjackInterface(){};
  RedjackInterface(std::vector<int> _channels, float _scanRate, ChannelBufferPtr _buffer);
	~RedjackInterface();

	/** Comms functions **/
	void openLabjack();
	void closeLabjack();
	bool isDeviceConnected();

	/** Calibration & configuration functions **/
	void getCalibrationInfo();
	//bool isCalibrationInfoValid();

	/** Streaming functions **/
	void StreamConfig();
	void StreamStart();
	void StreamStop();
	int  getStream(int *_packetCounter);
};

/* Unique ptr for interface
 * Uniqueness due to only one exisiting and is needed
 * for safe clean up handling
 */
typedef std::unique_ptr<RedjackInterface> RedjackIfacePtr;

#endif
