#ifndef BUFFER_HEADER
#define BUFFER_HEADER

#include <Sample.hpp>
#include <FIFO.hpp>
#include <pthread.h>


/* Thread-safe buffer implementation */
class Buffer : public FIFO{
public:
  /* Instantiates a buffer object with default
   * capacity of 20 elements
   */
  Buffer(size_t _capacity = 20):FIFO(_capacity){};
  ~Buffer();

  /* Push a samplePtr object into the buffer.
   * If buffer is full, a SamplePtr object is popped
   * otherwise, NULL SamplePtr is returned
   */
  SamplePtr push(SamplePtr _in);

  /* Pops a SamplePtr from the front of buffer
   * and moves it to current scope.
   */
  SamplePtr pop();

  /* Returns oldest Sample in a Sample copy */
  Sample getOldest();

  bool isFull();

  /* Following functions clear the buffer, either
   * entirely or for a given range within the 
   * structure.
   */
  void clear();

};

typedef std::unique_ptr<Buffer> BufferPtr;

#endif
