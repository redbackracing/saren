#ifndef COMPRESSOR_HEADER
#define COMPRESSOR_HEADER

#include <string>
#include <Sample.hpp>
#include <json.h>

/* This class is currently specifically implemented for Server Module
 * It accepts in a string and outputs a compressed string
 * It is expected this string will be used by the networking library to
 * transfer bytes across
 */
class Compressor
{
public:
  /* Takes in a string and compresses it */
  std::string compress(const std::string& uncompressed) const;

  /* Takes a JSON string and compresses it */
  std::string compress(const Json::Value& uncompressed) const;

  /* Takes a vector samples, converts it to JSON and compresses it */
  std::string compress(std::vector<Sample>& uncompressed);

  /* Decompress compressed string into original string */
  std::string decompress(const std::string& compressed) const;

private:
  /* Output of this function is just a JSON array of samples
   * [{Sample}]
   */
  Json::Value convertToJSON(std::vector<Sample>& input);

  /* Output of this function is a JSON object outlining:
   * Channel Name, Value, Time, Diag and Warn
   */
  Json::Value convertToJSON(Sample& input);

};

#endif