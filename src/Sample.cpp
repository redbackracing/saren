#include <Sample.hpp>
#include <sstream>
#include <cmath>
#include <json.h>
#include <iomanip>

#include <iostream>

Sample::Sample(){
  _channel.integer = -1;
  _value.decimal = -1.0;
  _time.decimal = -1.0;
  _diag = false;
  _warn = false;
  _name = "";
}

Sample::Sample(const uint32_t& channel, const float& value, const float& acqTime, const bool diag /*= false*/, const bool warn /*= false*/, const std::string name /*=""*/){
  _channel.integer = channel;
  _value.decimal = value;
  _time.decimal = acqTime;
  _diag = diag;
  _warn = warn;
  _name = name; 
}

void Sample::toString(std::string& output){
  std::stringstream ss;

  ss << "{" << getChannel() << "," << getValue() << "," << getTime() << "}";

  output = ss.str();
}

std::string Sample::toString(){
  std::string result;

  toString(result);

  return result;

}

void Sample::toJSONString2(std::string& output){

  std::stringstream ss;
  ss << "{\"channelname\":\"" << getName()
     << "\",\"channelnum\":" << getChannel()
     << ",\"diag\":";
  if (getDiag()){
    ss << "true";
  }
  else{
    ss << "false";
  }
  ss << ",\"time\":" << getTime()
     << ",\"value\":" << getValue()
     << ",\"warn\":" ;
  if (getWarn()){
    ss << "true";
  }
  else{
    ss << "false";
  }
  ss << "}\n";

  output = ss.str();
}

void Sample::toJSONString(std::string& output){
  Json::FastWriter writer;
    
  Json::Value container;
  container["channelnum"] = getChannel();
  container["channelname"] = getName();
  container["value"] = getValue();
  container["time"] =  getTime();
  container["diag"] = getDiag();
  container["warn"] = getWarn();
  
  output = writer.write(container);
}


std::string Sample::toJSONString(){
  Json::FastWriter writer;
    
  Json::Value container;
  container["channelnum"] = getChannel();
  container["channelname"] = getName();
  container["value"] = getValue();
  container["time"] = getTime();
  container["diag"] = getDiag();
  container["warn"] = getWarn();
  
  return writer.write(container);
}

	
bool Sample::equals(Sample* rhs){

  bool channelCheck = false;
  bool valueCheck = false;
  bool timeCheck = false;

  if(rhs != NULL){
    channelCheck = (getChannel() == rhs->getChannel());
    valueCheck = (getValue() == rhs->getValue());
    timeCheck = (getTime() == rhs->getTime());
  }
  
  //Don't check the warning and diag flags as value check will functionally account for that
  //If values are the same, then diag and warn flags should also be the same (if not we got bugs)
  return (channelCheck && valueCheck && timeCheck);
}

bool Sample::equals(Sample& rhs){

  bool channelCheck = false;
  bool valueCheck = false;
  bool timeCheck = false;

  channelCheck = (getChannel() == rhs.getChannel());
  valueCheck = (getValue() == rhs.getValue());
  timeCheck = (getTime() == rhs.getTime());
  
  //Don't check the warning and diag flags as value check will functionally account for that
  //If values are the same, then diag and warn flags should also be the same (if not we got bugs)
  return (channelCheck && valueCheck && timeCheck);
}

//Getters & Setters
std::string Sample::getName(){
  return _name;
}

void Sample::setName(const std::string& name){
  _name = name;
}

uint32_t Sample::getChannel(){
  return _channel.integer;
}

void Sample::setChannel(const uint32_t& channel){
  _channel.integer = channel;
}

float Sample::getValue(){
  round(_value.decimal);  
  return _value.decimal;
}

void Sample::setValue(const float& value){
  _value.decimal = value;
}

float Sample::getTime(){
  round(_time.decimal);
  return _time.decimal;
}

void Sample::setTime(const float& acqTime){
  _time.decimal = acqTime;
}

bool Sample::getDiag(){
  return _diag;
}

void Sample::setDiag(const bool& diag){
  _diag = diag;
}

bool Sample::getWarn(){
  return _warn;
}

void Sample::setWarn(const bool& warn){
  _warn = warn;
}

void Sample::round(float& ioFloat){
  ioFloat = (std::round(ioFloat*1000))/1000;
}
