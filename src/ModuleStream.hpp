#ifndef MODULE_STREAM_HEADER
#define MODULE_STREAM_HEADER

#include <vector>

#include <FIFO.hpp>
#include <Module.hpp>
#include <ConfigModule.hpp>
#include <ExceptionLogger.hpp>

/* Pure virtual class to represent a stream processing graph
 * This is the underlying structure of any of the data-logger designs
 * Designed as such to allow for different designs to be created into the future
 * Children have to implement the constructStream method to lay out the design
 */
class ModuleStream {
public:
  virtual void constructStream() = 0;

  /* Starts the module stream
   * Triggers each module inside _modules
   * Take a config file name as input, for ConfigModules to use
   * Returns false if unsuccessful, and rollbacks the started modules
   */
  virtual bool start(const std::string& iFilename);

  /* Stops currently running modules in stream
   * Returns true if successful
   */
  virtual bool stop();
  
  /* Essentially is a combined stop then start method
   * To allow new config files to be used on the fly
   */
  virtual bool reconfigure(const std::string& iFilename);

protected:
  std::vector<ModulePtr> _modules;
  std::vector<FIFOPtr> _fifos;
  bool _constructed = false;

private:
  ExceptionLogger& _exceptionLogger = ExceptionLogger::getInstance();
};

#endif