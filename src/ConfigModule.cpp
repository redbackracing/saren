#include <ConfigModule.hpp>
#include <ExceptionLogger.hpp>

ConfigModule::ConfigModule(const std::string& name, FIFOPtr input, FIFOPtr output) : Module(name,input,output) {
  //Set default configuration file
  _configFileName = "";
}

ConfigModule::~ConfigModule(){

}

std::string ConfigModule::getConfigFileName(){
  return _configFileName;
}

void ConfigModule::setConfigFileName(const std::string& iFilename){
  _configFileName = iFilename;
}

bool ConfigModule::startModule(){
  bool result = false;

  ConfigFile container;
  
  if(_cfp.parseConfig(_configFileName,container) == 0){
    constructConfiguration(container);
    result = true;
  } else {
    std::stringstream ss;
    ss << "Module " << _name << " couldn't parse file " << _configFileName;
    _exceptionLogger.writeLog(ss.str());
  }

  setRunning(result);

  //If parsing worked, reassign return of thread-create to result
  if(result){
    result = (pthread_create(&_thread,NULL,threadFunc,this) == 0);
  }

  return result;
}