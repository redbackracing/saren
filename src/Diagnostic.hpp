#ifndef DIAGNOSTIC_HEADER
#define DIAGNOSTIC_HEADER

#include <ConfigModule.hpp>
#include <map>

class Diagnostic : public ConfigModule {
public:
  Diagnostic(const std::string& name, FIFOPtr input, FIFOPtr output);
  ~Diagnostic();

  virtual SamplePtr process(SamplePtr _current);

protected:
  virtual void constructConfiguration(const ConfigFile& _container);

private:
  std::map<int,Range> ranges;

};

#endif