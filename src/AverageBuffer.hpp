#ifndef AVERAGE_BUFFER_HEADER
#define AVERAGE_BUFFER_HEADER

#include <Buffer.hpp>

class AverageBuffer : public Buffer {
public:
	AverageBuffer(size_t capacity = 20) : Buffer(capacity) {_sum = 0.0f;}

	/* Push a samplePtr object into the buffer.
   * If buffer is full, a SamplePtr object is popped
   * otherwise, NULL SamplePtr is returned
   */
  SamplePtr push(SamplePtr _in);

  /* Pops a SamplePtr from the front of buffer
   * and moves it to current scope.
   */
  SamplePtr pop();

  /* Computes the average sample by
   * Value: average of sample values inside buffer
   * Time: min time + (max-min)/2 time
   */
	SamplePtr averageSample();

private:

  //Running sum of values 
  float _sum;

};

typedef std::unique_ptr<AverageBuffer> AvgBufferPtr;

#endif