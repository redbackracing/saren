#ifndef WARNING_HEADER
#define WARNING_HEADER

#include <ConfigModule.hpp>
#include <map>

class Warning : public ConfigModule {
public:
  Warning(const std::string& name, FIFOPtr input, FIFOPtr output);
  ~Warning();

  virtual SamplePtr process(SamplePtr _current);

protected:
  virtual void constructConfiguration(const ConfigFile& _container);

private:
  std::map<int,Range> ranges;

};

#endif