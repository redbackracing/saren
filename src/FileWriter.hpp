#ifndef SAREN_FILEWRITER_H_
#define SAREN_FILEWRITER_H_

#include <ConfigModule.hpp>
#include <FileLogger.hpp>

class FileWriter : public Module{
  public:
    FileWriter(const std::string& name, FIFOPtr input, FIFOPtr output,bool Ybinary);
    ~FileWriter();
    virtual SamplePtr process(SamplePtr current);

  private:
    std::unique_ptr<FileLogger> _logger;
    ExceptionLogger& _exceptionLogger = ExceptionLogger::getInstance();
};

std::string getTimestampfilename(void);

#endif
