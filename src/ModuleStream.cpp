#include <ModuleStream.hpp>

bool ModuleStream::start(const std::string& iFilename){
  bool result = true;
  if(!_constructed){
    constructStream();
    _constructed = true;
  }

  std::vector<ModulePtr>::iterator iter;
  ModulePtr currentModule;
  ConfigModulePtr curConfigModule;

  for(iter = _modules.begin(); iter != _modules.end(); ++iter){
    currentModule = (*iter);

    //Check if module is a ConfigModule
    //Need to set config file name for Module
    
    curConfigModule = std::dynamic_pointer_cast<ConfigModule>(currentModule);
    if(curConfigModule.get() != NULL){
      curConfigModule->setConfigFileName(iFilename);
    }
    //Check if module is already running
    if(!currentModule->getRunning()){
      if(!currentModule->startModule()){
        std::stringstream ss;
        ss << "ModuleStream: Couldn't start " << currentModule->getName();
        _exceptionLogger.writeLog(ss.str());
        result = false;
        break;
      }
    }
  }

  if(!result){
    _exceptionLogger.writeLog("[MODULESTREAM]: Failed to start all modules.");
    stop();
  }

  return result;
}

bool ModuleStream::stop(){
  bool result = true;
  if(_constructed){
    std::vector<ModulePtr>::iterator iter;

    for(iter = _modules.begin(); iter != _modules.end(); ++iter){
      ModulePtr currentModule = (*iter);
      if(currentModule->getRunning()){ //If module is running
        if(!currentModule->stopModule()){ //Stop it
          std::stringstream ss;
          ss << "ModuleStream: Couldn't stop " << currentModule->getName();
          _exceptionLogger.writeLog(ss.str());
          result = false;
          break;
        }
      }
    }
  }

  return result;
}

bool ModuleStream::reconfigure(const std::string& iFilename){
  bool result = false;
  
  result = stop();

  if(result){
    result = start(iFilename);
  } else {
    _exceptionLogger.writeLog("ModuleStream: Start failed on reconfigure");
  }

  return result;
}
