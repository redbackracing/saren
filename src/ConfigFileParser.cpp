#include <ConfigFileParser.hpp>
#include <ExceptionLogger.hpp>

unsigned long ConfigFileParser::parseConfig(const std::string& iFilename, ConfigFile& oFile){

  std::string fullFileName = "config/" + iFilename + ".rbrc";

  _error.reset();
  std::ifstream rbrc;

  if(_error.count() == 0 && !readFile(fullFileName,rbrc)){
    _error[0] = true;
  }

  Json::Value root;
  if(_error.count() == 0 && !readJSON(rbrc,root)){
    _error[1] = true;
  }

  if(_error.count() == 0 && !readInputMap(root["inputs"],oFile.InputMap)){
    _error[2] = true;
  }

  if(_error.count() == 0 && !readOutput(root["outputs"],oFile.Output)){
    _error[3] = true;
  }  

  if(_error.to_ulong() > 0){
    ExceptionLogger& _exceptionLogger = ExceptionLogger::getInstance();
    std::stringstream ss;
    ss << matchError(_error.to_ulong()) << " on file " << fullFileName;
    _exceptionLogger.writeLog(ss.str());  
  }
  
  return _error.to_ulong();
}

std::string ConfigFileParser::matchError(const unsigned long& errorCode){
  std::string result;

  result += "ConfigFileParser: ";

  switch(errorCode){
    case BAD_FILE:
      result += "Bad file name, file doesn't exist";
      break;

    case BAD_JSON:
      result += "Bad JSON within file";
      break;

    case BAD_INPUT:
      result += "Check input map, one channel is bad";
      break;

    case BAD_OUTPUT:
      result += "Check output field, something is bad";
      break;

    default:
      break;
  }

  return result;
}

bool ConfigFileParser::readFile(const std::string& iFilename, std::ifstream& oFileStream){
  
  oFileStream.open(iFilename,std::ifstream::binary);

  return !oFileStream.fail();
}

bool ConfigFileParser::readJSON(std::ifstream& iFile, Json::Value& oValue){
  bool result = false;
  Json::Reader reader;

  result = reader.parse(iFile,oValue,false);

  oValue = oValue["configfile"];

  return result;
}

bool ConfigFileParser::readInputMap(const Json::Value& iRoot, ConfigInputMap& oInputMap){
  bool result = true;

  if(iRoot.isArray()){
    
    Json::Value::const_iterator iter = iRoot.begin();
    for(; iter != iRoot.end(); ++iter){
      
      std::pair<int,ConfigFileInput> curItem;
      
      result = readInput(*iter,curItem.second);
      
      if(result){

        curItem.first = curItem.second.channelNumber;
        oInputMap.insert(curItem);

      } else {

        break;

      }
    }
  }

  return result;
}

bool ConfigFileParser::readInput(const Json::Value& iRoot, ConfigFileInput& oInput){
  bool result = true;

  if(!iRoot.empty()){
    //Mandatory fields
    oInput.channelName = iRoot["channelName"].asString();
    oInput.channelNumber = readInt(iRoot["channelNumber"]);

    //Conditional fields
    oInput.samplingRate = readFloat(iRoot["samplingRate"]);
    oInput.readOrder = readInt(iRoot["readOrder"]);

    //Optional fields
    readRange(iRoot["valueRange"],oInput.valueRange);
    readRange(iRoot["signalRange"],oInput.signalRange);
    readConversion(iRoot["conversion"],oInput.convSet);

    //Perform mandatory field checks
    if(oInput.channelName == "" || oInput.channelNumber == -1){
      result = false;
    }  
  }
  
  return result;
}

bool ConfigFileParser::readOutput(const Json::Value& iRoot, ConfigFileOutput& oOutput){
  bool result = false;

  result = readVector(iRoot["live"],oOutput.live);

  return result;
}

bool ConfigFileParser::readVector(const Json::Value& iRoot, std::vector<int>& oOutput){
  bool result = true;

  if(iRoot.isArray()){
    
    Json::Value::const_iterator iter = iRoot.begin();

    for(; iter != iRoot.end(); ++iter){
      Json::Value current = (*iter);

      if(!current.empty()){
        int insert = readInt(current);
        if(insert == -1){
          result = false;
          oOutput.clear();
          break;
        } else {
          oOutput.push_back(insert);
        }
      }
    }
  }

  return result;
}

void ConfigFileParser::readRange(const Json::Value& iRoot, Range& oOutput){
  if(iRoot.isArray()){
    oOutput.first = readFloat(iRoot[0]);
    oOutput.second = readFloat(iRoot[1]);
  } else {
    oOutput.first = -1.0f;
    oOutput.second = -1.0f;
  }
}

void ConfigFileParser::readConversion(const Json::Value& iRoot, std::vector<Point>& oPoint){
  if(iRoot.isArray()){
    
    Json::Value::const_iterator iter = iRoot.begin();
    for(; iter != iRoot.end(); ++iter){
      Json::Value curPoint = (*iter);
      Point tmpPoint;
      tmpPoint.first = readFloat(curPoint["x"]);
      tmpPoint.second = readFloat(curPoint["y"]);
      oPoint.push_back(tmpPoint);
    }
  }
}

int ConfigFileParser::readInt(const Json::Value& iRoot){
  int result = -1;

  if(!iRoot.empty()){
    try {
      result = iRoot.asInt();  
    } catch (...){
      result = -1;
    }
  }

  return result;
}

float ConfigFileParser::readFloat(const Json::Value& iRoot){
  float result = -1;
  
  if(!iRoot.empty()){
    try {
      result = iRoot.asFloat();
    } catch(...){
      result = -1;
    }
  }

  return result;
}
