#ifndef MODULE_HEADER
#define MODULE_HEADER

#include <pthread.h>
#include <FIFO.hpp>
#include <memory>
#include <ExceptionLogger.hpp>

class Module {
public:
  Module(const std::string& name, FIFOPtr input, FIFOPtr output);
  ~Module();

  /* Thread control functions
   * Start and stop the thread running the module
   */
  virtual bool startModule();
  virtual bool stopModule();

  //Core processing functions
  virtual SamplePtr process(SamplePtr current);

  /* When overriding internal process
   * remember to output the SamplePtr in the function
   * as stateMachine doesn't do so automatically
   */ 
  virtual SamplePtr internalProcess();

  std::string getName();

  //Controls running flag of Module  
  bool getRunning();
  void setRunning(bool state);

protected:
  //The function used by the thread
  static void* threadFunc (void* thisModule);
  
  //State Machine of Module
  virtual void stateMachine();

  pthread_mutex_t _runningLock;
  std::string _name;
  bool _running;
  FIFOPtr _input;
  FIFOPtr _output;
  pthread_t _thread;

  ExceptionLogger& _exceptionLogger = ExceptionLogger::getInstance();

};

/* Smart pointer for Module
 * Uses a shared pointer as it may need to be casted
 * However, ideally, it should only have a single owner count
 */
typedef std::shared_ptr<Module> ModulePtr;

#endif
