#include <Compressor.hpp>
#include <snappy.h>


std::string Compressor::compress(const std::string& uncompressed) const{
  // compressing
  std::string compressed;
  snappy::Compress(uncompressed.c_str(), uncompressed.size(), &compressed);

  return compressed;
}

std::string Compressor::compress(const Json::Value& uncompressed) const{
  //Converting JSON to string
  Json::FastWriter jWriter;

  std::string to_compress = jWriter.write(uncompressed);

  // compressing
  std::string compressed = compress(to_compress);

  return compressed;
}

std::string Compressor::compress(std::vector<Sample>& uncompressed){
  //Converting sample vector to JSON
  Json::Value to_compress = convertToJSON(uncompressed);
  Json::Value data;
  data["data"] = to_compress;

  // compressing
  std::string compressed = compress(data);

  return compressed;
}

std::string Compressor::decompress(const std::string& compressed) const{
  std::string decompressed;

  snappy::Uncompress(compressed.c_str(),compressed.size(), &decompressed);
  
  return decompressed;
}

Json::Value Compressor::convertToJSON(std::vector<Sample>& input){
  Json::Value result(Json::arrayValue);

  for(int i = 0; i < input.size(); i++){
    result[i] = convertToJSON(input[i]);
  }

  return result;
}

Json::Value Compressor::convertToJSON(Sample& input){
  Json::Value result;

  result["channelName"] = input.getName();
  result["value"] = input.getValue();
  result["time"] = input.getTime();
  result["diag"] = input.getDiag();
  result["warn"] = input.getWarn();

  return result;
}
