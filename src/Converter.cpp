#include <Converter.hpp>
#include <ExceptionLogger.hpp>

#include <sstream>
#include <exception>

Converter::Converter(const std::string& name, FIFOPtr input, FIFOPtr output):ConfigModule(name,input,output){

}

Converter::~Converter(){

}

void Converter::constructConfiguration(const ConfigFile& _container){
  // Clears current conversion map  
  conversions.clear();

  // Map variable relating channel number to the input structure
  ConfigInputMap _inputMap(_container.InputMap);

  // Iterates through map of inputs to map channel number to conversion vector
  ConfigInputMap::iterator mapIter;
  for(mapIter = _inputMap.begin(); mapIter != _inputMap.end(); ++mapIter){
    if(!mapIter->second.convSet.empty()){
      conversions.insert(std::make_pair(mapIter->first,mapIter->second.convSet));
    }
  }
  return;
}

SamplePtr Converter::process(SamplePtr _current){

  uint32_t channelNumber(0);
  try{
    channelNumber = _current->getChannel();
  } catch(const std::out_of_range& oor){
      std::stringstream ss;
      ss << "[CONVERTER]: Channel " << _current->getChannel() << " not found.";
      _exceptionLogger.writeLog(ss.str());
      return std::move(_current);
  }

  if(channelNumber >= ANALOG_MIN && channelNumber <= ANALOG_MAX){
    _current = analogProcess(std::move(_current));
  }
  else if(channelNumber >= DIGITAL_MIN && channelNumber <= DIGITAL_MAX){
    _current = digitalProcess(std::move(_current));
  }
  else{
    _current = CANProcess(std::move(_current));
  }

  return std::move(_current);
}

SamplePtr Converter::analogProcess(SamplePtr _current){
  std::vector<Point> _conversion;
  
  try{
    // A vector holding the conversion map of the channel
    _conversion = conversions.at(_current->getChannel());
  } catch(const std::out_of_range& oor){
      // May need to add upperbound on channel number
      std::stringstream ss;
      ss << "[CONVERTER]: Channel #" << _current->getChannel() << " is out of channel range.";
      _exceptionLogger.writeLog(ss.str());
      return std::move(_current);
  }

  if(_conversion.empty()){
    return std::move(_current);
  }

  std::vector<Point>::iterator vectIter   = _conversion.begin();
  std::vector<Point>::iterator lowerBound = _conversion.end();
  std::vector<Point>::iterator upperBound = _conversion.end();
  float val = _current->getValue();

  try{
    for(; vectIter != _conversion.end(); ++vectIter){
      // Searches for lowerBound of the sample point
      if(val >= vectIter->first){
        lowerBound = vectIter;
      }
      // Searches for upperBound and only sets if lowerBound has been found
      if(val <= vectIter->first && lowerBound != _conversion.end()){
        upperBound = vectIter;
      }
      // If upper and lower bounds have been found - range has been found
      if(lowerBound != _conversion.end() && upperBound != _conversion.end()){
        break;
      }
    }
    if(lowerBound == _conversion.end() && upperBound == _conversion.end()){
      std::exception error;
      throw error;
    }
  } catch(std::exception& e){
      std::stringstream ss;
      ss << "[CONVERTER]: Range not found for value " << val << " for channel " << _current->getChannel();
      _exceptionLogger.writeLog(ss.str());
      return std::move(_current);
  }

  // Dereference bounded pair values
  Point lower = *(lowerBound);
  Point upper = *(upperBound);

  // Construct linear equation
  float result = val;
  if(upper.first != lower.first){
    float gradient = (upper.second - lower.second)/(upper.first - lower.first);
    float interval = (val - lower.first);
		result = gradient*interval + lower.second;
  }	else{
			result = upper.second;
	}
	_current->setValue(result);

	return std::move(_current);
}

SamplePtr Converter::digitalProcess(SamplePtr _current){
  std::vector<Point> _conversion;

  try{
    // A vector holding the conversion map of the channel
    _conversion = conversions.at(_current->getChannel());
  } catch(const std::out_of_range& oor){
      // May need to add upperbound on channel number
      std::stringstream ss;
      ss << "[CONVERTER]: Channel #" << _current->getChannel() << " is out of channel range.";
      _exceptionLogger.writeLog(ss.str());
      return std::move(_current);
  }

  if(_conversion.empty()){
    return std::move(_current);
  }
 
  float newBool(-1);
  float val(_current->getValue());
  std::vector<Point>::iterator vectIter   = _conversion.begin();
  
  try{
    // Exception thrown if boolean value is not in digital conversion map
    for(; vectIter != _conversion.end(); ++vectIter){
      if(vectIter->first == val){
        newBool = vectIter->second;
      }
    }
    if(newBool == -1){
          std::exception error;
          throw error;
    }
  } catch(std::exception& e){
      std::stringstream ss;
      ss << "[CONVERTER]: Boolean for channel " << _current->getChannel() << " is incorrect. Please specify 0 and 1 for digital conversions.";
      _exceptionLogger.writeLog(ss.str());
      return std::move(_current);
  }

  if(newBool != -1){
    _current->setValue(newBool);
  }

  return std::move(_current);
}
	
SamplePtr Converter::CANProcess(SamplePtr _current){
  std::vector<Point> _conversion;

  try{
    // A vector holding the conversion map of the channel
    _conversion = conversions.at(_current->getChannel());
  } catch(const std::out_of_range& oor){
      // May need to add upperbound on channel number
      std::stringstream ss;
      ss << "[CONVERTER]: Channel #" << _current->getChannel() << " is out of channel range.";
      _exceptionLogger.writeLog(ss.str());
      return std::move(_current);
  }

  if(_conversion.empty()){
    return std::move(_current);
  }  

  float canMultiplier = 0;

  try{
  // Exception thrown if boolean value is not in digital conversion map
    if(_conversion.front().first == _current->getValue()){
      canMultiplier = _conversion.front().second;
    } else{
        std::exception error;
        throw error;
    }
  } catch(std::exception& e){
    std::stringstream ss;
    ss << "[CONVERTER]: CAN Multiplier for channel " << _current->getChannel() << " not found.";
    _exceptionLogger.writeLog(ss.str());
    return std::move(_current);
  }

  _current->setValue(canMultiplier);

  return std::move(_current);
}










