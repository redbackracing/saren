#ifndef FILE_PTR_HEADER
#define FILE_PTR_HEADER

#include <memory>

class FileDeleter {
public:
  void operator()(FILE* f){
    if(f != NULL){
      fclose(f);
    }
  }
};

typedef std::unique_ptr<FILE,FileDeleter> FilePtr;

#endif