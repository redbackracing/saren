#ifndef SERVER_MODULE_HEADER
#define SERVER_MODULE_HEADER

#include <ConfigModule.hpp>
#include <ChannelBuffer.hpp>
#include <ExceptionLogger.hpp>
#include <Compressor.hpp>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <chrono>

using namespace std::chrono;

class Server : public ConfigModule {
public:
  Server(const std::string& name, FIFOPtr input, FIFOPtr output);

  /* Overriden module-specific process function
   * Pushes the Sample to the relevant Buffer
   * If it is pushed to a buffer, 
   * checks if a new server push needs to be done
   */
  virtual SamplePtr process(SamplePtr _current);

protected:
  virtual void constructConfiguration(const ConfigFile& _container);

private:

  void tryConnect();

  std::vector<int> _channels;
  std::map<uint32_t, std::string> _names;

  ChannelBufferPtr _buffer;

  time_point<steady_clock> _lastSentPush;

  //Socket File Descriptor
  //Linux provides an int to reference sockets
  int _sockfd;
  bool _connected;

  //Web Server address
  static std::string hostIP;
  static unsigned short port;
  struct sockaddr_in _serv_addr;

  Compressor _compressor;

  ExceptionLogger& _exceptionLogger = ExceptionLogger::getInstance();

};

#endif