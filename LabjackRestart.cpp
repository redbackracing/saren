#include <iostream>

#include <RedjackInterface.hpp>

int main(){
  RedjackInterface _iface;
  _iface.closeLabjack();

  if(!_iface.isDeviceConnected()){
    std::cout << "Restarting Labjack..." << std::endl;
  } else {
    std::cout << "Device could not be closed..." << std::endl;
    exit(EXIT_FAILURE);
  }

  _iface.openLabjack();

  if(_iface.isDeviceConnected()){
    std::cout << "Labjack Restarted!" << std::endl;
  } else {
    std::cout << "Device could not be restarted. Please try again later." << std::endl;
    return EXIT_SUCCESS;
  }

  return 0;
}
