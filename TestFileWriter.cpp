#include <FileWriter.hpp>
#include <Module.hpp>

#include <iostream>
#include <map>
#include <thread>
#include <chrono>
#include <cmath>
using namespace std;

int main(int argc, char* argv[]){
  int runTime = -1;

  if(argc > 1){
    runTime = atoi(argv[1]);
  }

  int numchannels = 12;
  FIFOPtr input(new FIFO(numchannels));
  FIFOPtr output;

  ModulePtr Wrtr(new FileWriter("FileWriter",input,output,false));

  Wrtr->startModule();
  
  SamplePtr insert;
  float randnum = 1;
  float b = 0;
  float time = 0.0000;
  bool diag = false;
  bool warn = false;
  char channelname[27] = "abcdefghijklmnopqrstuvwxyz";
  runTime =0 ;

  while(runTime > -1888){
    for(int i = 1; i <= numchannels; i++){
      b = rand() & 100 + 1; //do math
      diag = false;//do math
      randnum = rand() & 100;
      if (randnum >80){
        diag=true;
      }
      warn = false;
      randnum = rand() & 100;
      if (randnum >50){
        warn = true;
      }
      
      insert.reset(new Sample(i,b,time, diag, warn, std::to_string(channelname[i]-97)));
      input->addValue(std::move(insert));
    }
    
    //this_thread::sleep_for(chrono::seconds(1));

    time += 0.24351;
    runTime -= 1;
  }

  Wrtr->stopModule();
}
