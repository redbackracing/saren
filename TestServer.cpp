#include <Server.hpp>
#include <ConfigModule.hpp>

#include <iostream>
#include <map>
#include <thread>
#include <chrono>
#include <cmath>
using namespace std;

int main(int argc, char* argv[]){
  int runTime = -1;

  if(argc > 1){
    runTime = atoi(argv[1]);
  }

  FIFOPtr input(new FIFO(3));
  FIFOPtr output;

	ConfigModulePtr svr(new Server("Server",input,output));

  svr->setConfigFileName("testserver_config");

  svr->startModule();

  SamplePtr insert;
  float time = 0.0;
  while(runTime != 0){
    for(int i = 1; i <= 3; i++){
      insert.reset(new Sample(i,i,time));
      input->addValue(std::move(insert));
    }
    
    this_thread::sleep_for(chrono::seconds(1));

    time += 1.0;
    runTime--;
  }

  svr->stopModule();
}
