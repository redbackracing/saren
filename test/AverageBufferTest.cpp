#include <catch.hpp>
#include <AverageBuffer.hpp>

TEST_CASE("Check average sample works","[AverageBuffer]"){
	
  const int size = 10;

  AvgBufferPtr dut(new AverageBuffer(size));

  SamplePtr insert;
  SamplePtr expected(new Sample(1,4.5,4.5));

  for(int i = 0; i < size; i++){
    insert.reset(new Sample(1,i,i));

    dut->push(std::move(insert));

    REQUIRE(insert.get() == NULL);
  }

  SamplePtr actual = dut->averageSample();

  REQUIRE(dut->getSize() == 0);

  CHECK(actual->equals(expected.get()));

  //Re-populate, then pop one
  for(int i = 0; i < size; i++){
    insert.reset(new Sample(1,i,i));

    dut->push(std::move(insert));

    REQUIRE(insert.get() == NULL);
  }

  SamplePtr popped = dut->pop();

  expected.reset(new Sample(1,5,5));

  actual = dut->averageSample();

  REQUIRE(dut->getSize() == 0);

  CHECK(actual->equals(expected.get()));
}