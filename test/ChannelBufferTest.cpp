#include <catch.hpp>
#include <ChannelBuffer.hpp>

TEST_CASE("Can construct different channel buffers","[ChannelBuffer]"){
  ChannelBufferPtr dut;

  REQUIRE(dut.get() == NULL);

  SECTION("Set from 1 to 10"){
    std::vector<int> firstSet = {1,2,3,4,5,6,7,8,9,10};

    dut.reset(new ChannelBuffer(firstSet,20,false));

    for(size_t i = 0; i < firstSet.size(); i++){
      CHECK(dut->validChannel(firstSet[i]));
    }  
  }
  
  SECTION("Set of even from 2 to 10"){
    std::vector<int> evenSet = {2,4,6,8,10};

    dut.reset(new ChannelBuffer(evenSet,20,false));

    for(size_t i = 0; i < evenSet.size(); i++){
      CHECK(dut->validChannel(evenSet[i]));
    } 
  }
  

  SECTION("Set of odd from 1 to 9"){
    std::vector<int> oddSet = {1,3,5,7,9};

    dut.reset(new ChannelBuffer(oddSet,20,false));

    for(size_t i = 0; i < oddSet.size(); i++){
      CHECK(dut->validChannel(oddSet[i]));
    }  
  }
        
}

TEST_CASE("Able to push and pop from valid channels","[ChannelBuffer]"){
  SamplePtr expected(new Sample(1,1,1));
  SamplePtr actual(new Sample(1,1,1));

  ChannelBufferPtr dut;

  std::vector<int> channels = {1};
  size_t capacity = 1;

  dut.reset(new ChannelBuffer(channels,capacity,false));

  actual = dut->push(std::move(actual));

  REQUIRE(actual.get() == NULL);

  actual = dut->pop(1);

  REQUIRE(actual.get() != NULL);

  CHECK(actual->equals(expected.get()));

}

TEST_CASE("Retrieve null results on pushing and popping to invalid channels","[ChannelBuffer]"){
  ChannelBufferPtr dut;

  std::vector<int> channels = {1,2,3,4,5,6,7,8,9,10};
  size_t capacity = 10;


  dut.reset(new ChannelBuffer(channels,capacity,false));

  SamplePtr expected(new Sample(11,1,1));
  SamplePtr actual(new Sample(11,1,1));

  actual = dut->push(std::move(actual));

  REQUIRE(actual.get() != NULL);

  CHECK(actual->equals(expected.get()));

  SamplePtr nullPtr = dut->pop(1);

  REQUIRE(nullPtr.get() == NULL);

}

/* This test case is semi-geared towards Server module */
TEST_CASE("Can retrieve a snapshot from all channels","[ChannelBuffer]"){
  ChannelBufferPtr dut;

  std::vector<int> channels = {1,2,3,4};
  size_t capacity = 1;

  dut.reset(new ChannelBuffer(channels,capacity,false));

  std::vector<Sample> expected;
  std::vector<Sample> actual;
  SamplePtr insert;

  for(int i = 1; i <= 4; i++){
    insert.reset(new Sample(i,i,i));
    expected.push_back(*(insert.get()));
    dut->push(std::move(insert));
    REQUIRE(insert.get() == NULL);
  }

  dut->snapShot(actual);

  REQUIRE(actual.size() == expected.size());

  for(size_t i = 0; i < expected.size(); i++){
    CHECK(expected[i].equals(actual[i]));
  }
}

TEST_CASE("Able to receive sample based on push overflow on certain buffer","[ChannelBuffer]"){
  ChannelBufferPtr dut;

  std::vector<int> channels = {1};
  size_t capacity = 1;

  dut.reset(new ChannelBuffer(channels,capacity,false));

  SamplePtr expected(new Sample(1,1,1));

  SamplePtr actual(new Sample(1,1,1));

  actual = dut->push(std::move(actual));

  REQUIRE(actual.get() == NULL);

  actual.reset(new Sample(1,2,2));

  actual = dut->push(std::move(actual));

  REQUIRE(actual.get() != NULL);

  CHECK(actual->equals(expected.get()));

}

TEST_CASE("Can construct different channel average buffers","[ChannelBuffer]"){
  ChannelBufferPtr dut;

  REQUIRE(dut.get() == NULL);

  SECTION("Set from 1 to 10"){
    std::vector<int> firstSet = {1,2,3,4,5,6,7,8,9,10};

    dut.reset(new ChannelBuffer(firstSet,20,true));

    for(size_t i = 0; i < firstSet.size(); i++){
      CHECK(dut->validChannel(firstSet[i]));
    }  
  }
  
  SECTION("Set of even from 2 to 10"){
    std::vector<int> evenSet = {2,4,6,8,10};

    dut.reset(new ChannelBuffer(evenSet,20,true));

    for(size_t i = 0; i < evenSet.size(); i++){
      CHECK(dut->validChannel(evenSet[i]));
    } 
  }
  

  SECTION("Set of odd from 1 to 9"){
    std::vector<int> oddSet = {1,3,5,7,9};

    dut.reset(new ChannelBuffer(oddSet,20,true));

    for(size_t i = 0; i < oddSet.size(); i++){
      CHECK(dut->validChannel(oddSet[i]));
    }  
  }
}

TEST_CASE("Able to retrieve an average from all average buffers","[ChannelBuffer]"){
  ChannelBufferPtr dut;

  std::vector<int> channels = {1,2,3,4,5,6,7,8,9,10};
  size_t capacity = 10;


  dut.reset(new ChannelBuffer(channels,capacity,true));

  //Populates each buffer with samples with values 0-9
  for(size_t i = 0; i < channels.size(); i++){
    for(size_t j = 0; j < capacity; j++){
      SamplePtr insert(new Sample(channels[i],j,j));
      dut->push(std::move(insert));
      REQUIRE(insert.get() == NULL);
    }
  }

  //Checks each individual channel average sample
  //Should all be {channel#,4.5,4.5}
  SamplePtr expected;
  SamplePtr actual;
  for(size_t i = 0; i < channels.size(); i++){
    expected.reset(new Sample(channels[i],4.5,4.5));

    actual = dut->averageSample(channels[i]);

    REQUIRE(actual.get() != NULL);

    CHECK(actual->equals(expected.get()));
  }
}

