#include <catch.hpp>
#include <ModuleStream.hpp>

//Test class implementation to check constructConfiguration works
//Copied from ConfigModule Test
class TestConfigModule : public ConfigModule {
public:

  TestConfigModule(const std::string& name, FIFOPtr input, FIFOPtr output) : ConfigModule(name,input,output) {}

  virtual void constructConfiguration(const ConfigFile& iCf){
    //Takes the first element in each configurabtion file inputs
    if(iCf.InputMap.size() > 0){
      std::pair<int,ConfigFileInput> item = *(iCf.InputMap.begin());
      ConfigFileInput firstChannel = item.second;
      _channelName = firstChannel.channelName;
      _channelNumber = firstChannel.channelNumber;
      _samplingRate = firstChannel.samplingRate;
      _readOrder = firstChannel.readOrder;  
    } else {
      _channelName = "";
      _channelNumber = -1;
      _samplingRate = -1;
      _readOrder = -1;
    }
  }

  std::string _channelName;
  int _channelNumber;
  int _samplingRate;
  int _readOrder;

};

class TestModuleStream : public ModuleStream {

public:
  //The stream will look as follows
  // | Dummy 1 | -> 
  //                | Other | -> | Something |
  // | Dummy 2 | ->
  virtual void constructStream(){
    FIFOPtr inputToOther(new FIFO());
    FIFOPtr inputToSomething(new FIFO());
    
    _fifos.push_back(inputToOther);
    _fifos.push_back(inputToSomething);

    FIFOPtr nullFIFO;

    ModulePtr Dummy1(new Module("Dummy 1", nullFIFO, inputToOther));
    ModulePtr Dummy2(new TestConfigModule("Dummy 2", nullFIFO, inputToOther));
    ModulePtr Other(new Module("Other", inputToOther, inputToSomething));
    ModulePtr Something(new Module("Something", inputToSomething, nullFIFO));

    _modules.push_back(Dummy1);
    _modules.push_back(Dummy2);
    _modules.push_back(Other);
    _modules.push_back(Something);
  }

};

TEST_CASE("Able to start and stop modules","[ModuleStream]"){
  TestModuleStream dut;

  REQUIRE(dut.start("configmodule_start"));

  REQUIRE(dut.stop());

  REQUIRE(dut.start("configmodule_start"));

  REQUIRE(dut.stop());  

}

TEST_CASE("Able to reconfigure modules","[ModuleStream]"){
  TestModuleStream dut;

  REQUIRE(dut.start("configmodule_start"));

  REQUIRE(dut.reconfigure("configmodule_reconfig"));

  REQUIRE(dut.stop());
}

