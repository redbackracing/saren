#include <catch.hpp>
#include <Converter.hpp>

#include <thread>
#include <chrono>

TEST_CASE("Edge point testing for analogProcess","[Converter]"){
  FIFOPtr input(new FIFO());
  FIFOPtr output(new FIFO());

  SamplePtr test1(new Sample(1,1,1));
  SamplePtr test2(new Sample(1,3.3,1));

  ConfigModulePtr dut(new Converter("Converter",input,output));

  dut->setConfigFileName("Converter");

  // Starts thread for processing
  REQUIRE(dut->startModule() == true);

  REQUIRE(test1.get() != NULL);
  REQUIRE(test2.get() != NULL);

  // Adding samples to the input FIFO
  input->addValue(std::move(test1));
  input->addValue(std::move(test2));

  // Processing by independent thread whenever there is samples in the input FIFO
  // Processed samples are mved back into respective variables
  test1 = output->getNextValue();
  test2 = output->getNextValue();

  REQUIRE(test1.get() != NULL);
  REQUIRE(test2.get() != NULL);

  CHECK(test1->getValue() == 10);
  CHECK(test2->getValue() == 2.5f);

  REQUIRE(dut->stopModule() == true);
}
 
TEST_CASE("Interpolation point testing for analogProcess","[Converter]"){
  FIFOPtr input(new FIFO());
  FIFOPtr output(new FIFO());

  SamplePtr test1(new Sample(1,0.5,1));
  SamplePtr test2(new Sample(1,4,1));

  ConfigModulePtr dut(new Converter("Converter",input,output));

  dut->setConfigFileName("Converter");

  REQUIRE(dut->startModule() == true);

  REQUIRE(test1.get() != NULL);
  REQUIRE(test2.get() != NULL);

  // Adding samples to the input FIFO
  input->addValue(std::move(test1));
  input->addValue(std::move(test2));

  // Processing by independent thread whenever there is samples in the input FIFO
  // Processed samples are mved back into respective variables
  test1 = output->getNextValue();
  test2 = output->getNextValue();

  REQUIRE(test1.get() != NULL);
  REQUIRE(test2.get() != NULL);

  CHECK(test1->getValue() == 15);
  CHECK(test2->getValue() == 1.771f);

  REQUIRE(dut->stopModule() == true);
}

TEST_CASE("Checking positive and negative digital conversion works","[Converter]"){
  FIFOPtr input(new FIFO());
  FIFOPtr output(new FIFO());

  SamplePtr test1(new Sample(15,0,1));
  SamplePtr test2(new Sample(15,1,1));

  SamplePtr test3(new Sample(16,0,1));
  SamplePtr test4(new Sample(16,1,1));

  ConfigModulePtr dut(new Converter("Converter",input,output));

  dut->setConfigFileName("Converter");

  REQUIRE(dut->startModule() == true);
  
  REQUIRE(test1.get() != NULL);
  REQUIRE(test2.get() != NULL);
  
  REQUIRE(test3.get() != NULL);
  REQUIRE(test4.get() != NULL);

  // Adding samples to the input FIFO
  input->addValue(std::move(test1));
  input->addValue(std::move(test2));

  input->addValue(std::move(test3));
  input->addValue(std::move(test4));

  // Processing by independent thread whenever there is samples in the input FIFO
  // Processed samples are mved back into respective variables
  test1 = output->getNextValue();
  test2 = output->getNextValue();

  test3 = output->getNextValue();
  test4 = output->getNextValue();

  REQUIRE(test1.get() != NULL);
  REQUIRE(test2.get() != NULL);
  
  REQUIRE(test3.get() != NULL);
  REQUIRE(test4.get() != NULL);

  CHECK(test1->getValue() == 0);
  CHECK(test2->getValue() == 1);

  CHECK(test3->getValue() == 1);
  CHECK(test4->getValue() == 0);

  REQUIRE(dut->stopModule() == true);
}

TEST_CASE("Checking CAN conversion works","[Converter]"){
  FIFOPtr input(new FIFO());
  FIFOPtr output(new FIFO());

  SamplePtr test(new Sample(36,1,1));

  ConfigModulePtr dut(new Converter("Converter",input,output));

  dut->setConfigFileName("Converter");

  REQUIRE(dut->startModule() == true);
  
  REQUIRE(test.get() != NULL);

  input->addValue(std::move(test));
  test = output->getNextValue();

  REQUIRE(test.get() != NULL);

  CHECK(test->getValue() == 10);

  REQUIRE(dut->stopModule() == true);
}

TEST_CASE("Checking Converter does not process for a channel input with no conversion map","[Converter]"){
  FIFOPtr input(new FIFO());
  FIFOPtr output(new FIFO());

  SamplePtr test(new Sample(40,33,1));

  ConfigModulePtr dut(new Converter("Converter",input,output));

  dut->setConfigFileName("Converter");

  REQUIRE(dut->startModule() == true);
  
  REQUIRE(test.get() != NULL);

  input->addValue(std::move(test));
  test = output->getNextValue();

  REQUIRE(test.get() != NULL);

  // No processing is done since channel input did not contain conv. map
  CHECK(test->getValue() == 33);

  REQUIRE(dut->stopModule() == true);
  
}
