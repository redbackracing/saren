#include <catch.hpp>
#include <Sample.hpp>

/* For float comparisons, the numbers use a suffix 'f' to
 * specifically indicate it's a float, otherwise C++ naturally
 * uses double
 */

TEST_CASE("Checking general constructor works","[Sample]"){
	SamplePtr test(new Sample(1,1,1));

	REQUIRE(test.get() != NULL);

	CHECK(test->getChannel() == 1);
	CHECK(test->getValue() == 1.0f);
	CHECK(test->getTime() == 1.0f);

	SECTION("and old pointer doesn't memory leak if changed"){
		//Valgrind will check the pointer isn't leaked
		test.reset(new Sample(1,2,3));

		REQUIRE(test.get() != NULL);

		CHECK(test->getChannel() == 1);
		CHECK(test->getValue() == 2.0f);
		CHECK(test->getTime() == 3.0f);

	}
}

TEST_CASE("Checking toString is correct","[Sample]"){
	std::string expected = "{1,2.504,2.555}";
	SamplePtr test(new Sample(1,2.504,2.555111));
	std::string actual;

	REQUIRE(test.get() != NULL);
	test->toString(actual);

	CHECK(actual == expected);

  actual = test->toString();

  CHECK(actual == expected);
}

TEST_CASE("Checking JSONtoString is correct","[Sample]"){// JSONtostring may not be used, it definitley isn't used by filewriter
	std::string expected = "{1,2.504,2.555}";
	SamplePtr test(new Sample(1,2.5045678,2.555111));
	std::string actual;

	REQUIRE(test.get() != NULL);
	test->toJSONString(actual);

	//CHECK(actual == expected);

  actual = test->toJSONString();

  //CHECK(actual == expected);
}

TEST_CASE("Checking JSONtoString2 (the one in filewriter) is correct","[Sample]"){
  SamplePtr test(new Sample(1,2.50645678,2.5551111));
  std::string expected = "{\"channelname\":\"\",\"channelnum\":1,\"diag\":false,\"time\":2.555,\"value\":2.506,\"warn\":false}\n";
  std::string actual;
  REQUIRE(test.get() != NULL);
  test->toJSONString2(actual);
  CHECK(actual == expected);
}

TEST_CASE("Checking equals is correct","[Sample]"){
	SamplePtr one(new Sample(1,1,1));
	SamplePtr two(new Sample(1,1.0,1.0));

	REQUIRE(one.get() != NULL);
	REQUIRE(two.get() != NULL);

	CHECK(one->equals(two.get()) == true);

	two.reset(new Sample(1,1.5,1.5));

	REQUIRE(two.get() != NULL);

	CHECK(one->equals(two.get()) == false);

	two.reset();

	REQUIRE(two.get() == NULL);

	CHECK(one->equals(two.get()) == false);
}

TEST_CASE("Checking the getters for floats are rounded","[Sample]"){
	SamplePtr test(new Sample(1,1.5,1.5));

	REQUIRE(test.get() != NULL);

	CHECK(test->getChannel() == 1);
	CHECK(test->getValue() == 1.5f);
	CHECK(test->getTime() == 1.5f);

	test->setValue(1.5067);
	CHECK(test->getValue() == 1.507f);

	test->setTime(1.5062);
	CHECK(test->getTime() == 1.506f);

	test->setValue(1.78670809);
	CHECK(test->getValue() == 1.787f);

	test->setTime(1.786297234);
	CHECK(test->getTime() == 1.786f);	

}
