#include <catch.hpp>
#include <Module.hpp>

//Test Module which allows the test to check internalProcess works
class TestInternalModule : public Module {
public:
  TestInternalModule(const std::string name, FIFOPtr input, FIFOPtr output) : Module(name,input,output) {}
  ~TestInternalModule(){}

  virtual SamplePtr internalProcess(){
    static int counter = 1;
    
    if(counter > 0){
      SamplePtr _internal(new Sample(counter,counter,counter));
      counter--;
      return std::move(_internal);
    }

    return NULL;
  }
};

TEST_CASE("Can pass through a SamplePtr in a dummy module", "[Module]"){
  FIFOPtr input(new FIFO());
  FIFOPtr output(new FIFO());
  ModulePtr dut(new Module("Test", input, output));

  SamplePtr expected(new Sample(1,1,1));
  input->addValue(SamplePtr(new Sample(1,1,1)));

  REQUIRE(dut->startModule());

  SamplePtr actual = output->getNextValue();

  REQUIRE(dut->stopModule());

  CHECK(expected->equals(actual.get()));
}

TEST_CASE("Internal Process works", "[Module]"){
  FIFOPtr input(new FIFO());
  FIFOPtr output(new FIFO());
  ModulePtr dut(new TestInternalModule("TestInternal", input, output));

  SamplePtr expected(new Sample(1,1,1));
  REQUIRE(dut->startModule());

  SamplePtr actual = output->getNextValue();

  REQUIRE(dut->stopModule());

  CHECK(expected->equals(actual.get()));
}
