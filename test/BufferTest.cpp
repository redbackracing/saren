#include <catch.hpp>
#include <Buffer.hpp>

TEST_CASE("Check different size buffers can be initialised","[Buffer]"){
	BufferPtr test(new Buffer(1000));

  CHECK(test->getCapacity() == 1000);
}

TEST_CASE("Check push overflow returns the first sample pushed in","[Buffer]"){
  
  const int size = 20;

  BufferPtr test(new Buffer(size));

  SamplePtr actual;
  SamplePtr expected(new Sample(0,0,0));
  SamplePtr insert;

  for(size_t i = 0; i < size+1; i++){
    insert = SamplePtr(new Sample(i,i,i));
    actual = test->push(std::move(insert));

    if(i < size){
      CHECK(actual.get() == NULL);
    }
    
  }

  REQUIRE(actual.get() != NULL);

  actual->equals(expected.get());

}

TEST_CASE("Check pop works","[Buffer]"){
  
  BufferPtr test(new Buffer(1));

  SamplePtr actual(new Sample(0,0,0));
  SamplePtr expected(new Sample(0,0,0));
  
  test->push(std::move(actual));

  REQUIRE(actual.get() == NULL);

  CHECK(test->isFull() == true);

  actual = test->pop();

  REQUIRE(actual.get() != NULL);

  actual->equals(expected.get());

}
