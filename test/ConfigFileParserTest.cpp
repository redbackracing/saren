#include <catch.hpp>
#include <ConfigFileParser.hpp>

bool checkPairEquality(std::pair<float,float>& lhs, std::pair<float,float>& rhs){
  bool firstCheck = (lhs.first == rhs.first);
  bool secondCheck = (lhs.second == rhs.second);

  return firstCheck && secondCheck;
}

TEST_CASE("Checks and handles bad file input correctly","[ConfigFileParser]"){
  ConfigFileParser cfp;
  ConfigFile cf;

  REQUIRE(cfp.parseConfig("non-existant",cf) == 1);
}

TEST_CASE("Checks and handles bad json input correctly","[ConfigFileParser]"){
  ConfigFileParser cfp;
  ConfigFile cf;

  REQUIRE(cfp.parseConfig("bad_json",cf) == 2);
}

TEST_CASE("Checks and handles bad input correctly","[ConfigFileParser]"){
  ConfigFileParser cfp;
  ConfigFile cf;

  REQUIRE(cfp.parseConfig("bad_input",cf) == 4);
}

TEST_CASE("Checks and handles bad output correctly","[ConfigFileParser]"){
  ConfigFileParser cfp;
  ConfigFile cf;

  REQUIRE(cfp.parseConfig("bad_output",cf) == 8);
}

TEST_CASE("Checks and handles good file","[ConfigFileParser]"){
  ConfigFileParser cfp;
  ConfigFile cf;

  REQUIRE(cfp.parseConfig("good",cf) == 0);

  std::pair<float,float> refValueRange(1.5,3.4);
  std::pair<float,float> refSignalRange(0.5,5);

  std::vector<Point> refAnalogConversion { Point(1,2), Point(2.5,2.5), Point(5,3)};

  Point refCANConversion(1,10);

  //Check Channel 1
  CHECK(cf.InputMap[1].channelName == "Test 1");
  CHECK(cf.InputMap[1].channelNumber == 1);
  CHECK(cf.InputMap[1].samplingRate == 1000.0f);
  CHECK(cf.InputMap[1].readOrder == -1);
  CHECK(checkPairEquality(cf.InputMap[1].valueRange,refValueRange));
  CHECK(checkPairEquality(cf.InputMap[1].signalRange,refSignalRange));

  for(int i = 0; i < 3; i++){
    CHECK(checkPairEquality(cf.InputMap[1].convSet[i],refAnalogConversion[i]));
  }

  //Check Channel 36
  CHECK(cf.InputMap[36].channelName == "CAN");
  CHECK(cf.InputMap[36].channelNumber == 36);
  CHECK(cf.InputMap[36].samplingRate == -1.0f);
  CHECK(cf.InputMap[36].readOrder == 6);
  CHECK(checkPairEquality(cf.InputMap[36].valueRange,refValueRange));
  CHECK(checkPairEquality(cf.InputMap[36].signalRange,refSignalRange));
  CHECK(checkPairEquality(cf.InputMap[36].convSet[0], refCANConversion));

  //Check Output
  CHECK(cf.Output.live[0] == 1);
  CHECK(cf.Output.live[1] == 36);
}
