#include <catch.hpp>
#include <Compressor.hpp>

#include <sstream>

TEST_CASE("text can be compressed and decompressed","[Compressor]"){
  Compressor dut;

  std::string expected = "{\"greatest text ever\": 3}";

  std::string compressed = dut.compress(expected);

  std::string actual = dut.decompress(compressed);

  CHECK(expected == actual);
}

TEST_CASE("JSON values can be compressed","[Compressor]"){
  Compressor dut;

  Json::Value input;
  int value = 3;
  input["greatest text ever"] = value;

  std::string expected = "{\"greatest text ever\":3}\n";

  std::string compressed = dut.compress(input);

  std::string actual = dut.decompress(compressed);

  CHECK(expected == actual);
}

TEST_CASE("Vector of samples can be compressed","[Compressor]"){
  std::vector<Sample> input;
  Compressor dut;

  std::string expected = "{\"data\":[{\"channelName\":\"Channel 1\",\"diag\":true,\"time\":1,\"value\":1,\"warn\":true},{\"channelName\":\"Channel 2\",\"diag\":true,\"time\":2,\"value\":2,\"warn\":true},{\"channelName\":\"Channel 3\",\"diag\":true,\"time\":3,\"value\":3,\"warn\":true}]}\n";

  for(int i = 1; i <= 3; i++){
    std::stringstream ss;
    ss << "Channel " << i;
    Sample insert(i,i,i,true,true,ss.str());
    input.push_back(insert);
  }

  std::string compressed = dut.compress(input);

  std::string actual = dut.decompress(compressed);

  CHECK(expected == actual);

}
