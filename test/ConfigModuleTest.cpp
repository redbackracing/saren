#include <catch.hpp>
#include <ConfigModule.hpp>

#include <thread>
#include <chrono>

//Test class implementation to check constructConfiguration works
class TestConfigModule : public ConfigModule {
public:

  TestConfigModule(const std::string& name, FIFOPtr input, FIFOPtr output) : ConfigModule(name,input,output) {}

  virtual void constructConfiguration(const ConfigFile& iCf){
    //Takes the first element in each configurabtion file inputs
    if(iCf.InputMap.size() > 0){
      std::pair<int,ConfigFileInput> item = *(iCf.InputMap.begin());
      ConfigFileInput firstChannel = item.second;
      _channelName = firstChannel.channelName;
      _channelNumber = firstChannel.channelNumber;
      _samplingRate = firstChannel.samplingRate;
      _readOrder = firstChannel.readOrder;  
    } else {
      _channelName = "";
      _channelNumber = -1;
      _samplingRate = -1;
      _readOrder = -1;
    }
  }

  std::string _channelName;
  int _channelNumber;
  int _samplingRate;
  int _readOrder;

};

TEST_CASE("Able to specify config file on startup","[ConfigModule]"){
  FIFOPtr input(new FIFO());
  FIFOPtr output(new FIFO());
  std::unique_ptr<TestConfigModule> dut(new TestConfigModule("Test",input,output));

  SECTION("Able to start the module"){
    dut->setConfigFileName("configmodule_start");
    
    REQUIRE(dut->startModule() == true);

    //Wait for configuration to take place
    std::this_thread::sleep_for (std::chrono::seconds(1));

    CHECK(dut->_channelName == "Test 1");
    CHECK(dut->_channelNumber == 1);
    CHECK(dut->_samplingRate == 1000.0f);
    CHECK(dut->_readOrder == -1);
    
    REQUIRE(dut->stopModule() == true);
  }

  SECTION("Able to reconfigure the module"){
    dut->setConfigFileName("configmodule_reconfig");

    REQUIRE(dut->startModule() == true);

    //Wait for configuration to take place
    std::this_thread::sleep_for (std::chrono::seconds(1));

    CHECK(dut->_channelName == "CAN");
    CHECK(dut->_channelNumber == 36);
    CHECK(dut->_samplingRate == -1.0f);
    CHECK(dut->_readOrder == 6);

    REQUIRE(dut->stopModule() == true);
  }
}

TEST_CASE("Can be type checked as a ConfigModulePtr from a ModulePtr","[ConfigModule]"){
  FIFOPtr input(new FIFO());
  FIFOPtr output(new FIFO());
  ModulePtr dut(new TestConfigModule("Test",input,output));

  REQUIRE(std::dynamic_pointer_cast<ConfigModule>(dut).get() != NULL);
}

TEST_CASE("Running is false, when bad config file is parsed", "[ConfigModule]"){
  FIFOPtr input(new FIFO());
  FIFOPtr output(new FIFO());
  ConfigModulePtr dut(new TestConfigModule("Test", input, output));

  dut->setConfigFileName("bad_input");

  REQUIRE(dut->startModule() == false);
}
