#include <catch.hpp>
#include <FileWriter.hpp>
#include <cstdint>
#include <vector>
#include <string>
#include <FilePtr.hpp>
#include <ctime>
#include <iostream>

TEST_CASE("Testing a single data sample file(string)","[FileWriter]"){

  FIFOPtr input(new FIFO());
  FIFOPtr output(new FIFO());
  std::string filename = getTimestampfilename();
  ModulePtr dut(new FileWriter("FilerWriter",input,output,false));
  SamplePtr tstSample(new Sample(2,1,1,false,false,"test"));
  std::string expected;
  expected ="{\"channelname\":\"test\",\"channelnum\":2,\"diag\":false,\"time\":1,\"value\":1,\"warn\":false}\n";
  CHECK(!(tstSample == NULL));

  tstSample = (dut->process(std::move(tstSample)));

  CHECK((tstSample == NULL));

  dut.reset();
  dut = NULL;
   
  FilePtr testdatafile(fopen(filename.c_str(),"r"));
  fseek(testdatafile.get(), 0, SEEK_END);
  long lSize = ftell(testdatafile.get());
  rewind(testdatafile.get());
  
  std::string actualStr(lSize,' ');
  REQUIRE(fread(&actualStr[0],1,lSize,testdatafile.get()) == lSize);
  
  CHECK(actualStr==expected);
  
  remove(filename.c_str());
}
