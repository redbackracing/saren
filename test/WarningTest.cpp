#include <catch.hpp>
#include <Warning.hpp>

#include <thread>
#include <chrono>



TEST_CASE("Testing that process is checking ranges correctly for Warning","[Warning]"){
  FIFOPtr input(new FIFO());
  FIFOPtr output(new FIFO());

  SamplePtr test1(new Sample(1,1,1));
  SamplePtr test2(new Sample(1,1.5,1));
  SamplePtr test3(new Sample(1,2.6,1));
  SamplePtr test4(new Sample(1,3.6,1));
  SamplePtr test5(new Sample(15,1,1));


  ConfigModulePtr dut(new Warning("Warning",input,output));

  dut->setConfigFileName("Warning");

  // Starts thread for processing
  REQUIRE(dut->startModule() == true);
  
  REQUIRE(test1.get() != NULL);
  REQUIRE(test2.get() != NULL);
  REQUIRE(test2.get() != NULL);
  REQUIRE(test2.get() != NULL);
  REQUIRE(test2.get() != NULL);



  // Adding samples to the input FIFO
  input->addValue(std::move(test1));
  input->addValue(std::move(test2));
  input->addValue(std::move(test3));
  input->addValue(std::move(test4));
  input->addValue(std::move(test5));

  // Processing by independent thread whenever there is samples in the input FIFO
  // Processed samples are mved back into respective variables
  test1 = output->getNextValue();
  test2 = output->getNextValue();
  test3 = output->getNextValue();
  test4 = output->getNextValue();
  test5 = output->getNextValue();
  
  
  CHECK(test1.get() != NULL);
  CHECK(test2.get() != NULL);
  CHECK(test3.get() != NULL);
  CHECK(test4.get() != NULL);
  CHECK(test5.get() != NULL);
  
  
  CHECK(test1->getWarn() == true);
  
  CHECK(test2->getWarn() == false);
  
  CHECK(test3->getWarn() == false);
  
  CHECK(test4->getWarn() == true);
  
  CHECK(test5->getWarn() == true);
  
  REQUIRE(dut->stopModule() == true);
}