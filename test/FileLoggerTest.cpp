#include <catch.hpp>
#include <FileLogger.hpp>
#include <cstdint>
#include <vector>


TEST_CASE("Testing text file initialisation and writing to it","[FileLogger]"){
    const std::string filename = "test1.txt";
    
    //Creates test file
    FileLogger* dut = new FileLogger(filename);
    
    //Writes text to file
    std::string expected = "First text";
    CHECK(dut->write(expected) == true);
    
    //Deletes logging object but shouldn't delete file
    delete dut;
    
    //Validate what was written
    FILE* test = fopen(filename.c_str(),"r");
    char* testString = (char*) malloc(10);
    fread(testString,1,10,test);
    
    //Need to specify size of string as string isn't null terminated
    std::string actual(testString,10);
    
    CHECK(actual == expected);
    
    //Deletes the file
    remove(filename.c_str());
}

TEST_CASE("Testing binary file initialisation and writing to it","[FileLogger]"){
    const std::string filename = "test2.bin";
    
    //Creates binary test file
    FileLogger* dut = new FileLogger(filename,true);
    
    //Writes binary to file
    std::vector<uint8_t> expected = {1,2,3,4,5,6,7,8,9,10};
    CHECK(dut->write(expected) == true);
    
    //Deletes logging object but shouldn't delete file
    delete dut;
    
    //Validate what was written
    FILE* test = fopen(filename.c_str(),"rb");
    char actual[10];
    fread(actual,1,10,test);
    
    for(int i = 0; i < 10; i++){
        CHECK(actual[i] == expected[i]);
    }
    
    //Deletes the file
    remove(filename.c_str());
}