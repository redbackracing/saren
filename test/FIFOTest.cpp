#include <catch.hpp>
#include <Sample.hpp>
#include <FIFO.hpp>

TEST_CASE("FIFO needs to be able to add and remove samples", "[FIFO]"){

  FIFOPtr dut(new FIFO());

  REQUIRE(dut.get() != NULL);

  SamplePtr ref;
  SamplePtr test;
  
  SECTION("Adding samples to FIFO"){
    for(int i = 0; i < 20; i++){
      CHECK(dut->getSize() == i);      
      test.reset(new Sample(i,i,i));

      REQUIRE(test.get() != NULL);
      dut->addValue(std::move(test));
      REQUIRE(test.get() == NULL);      
    }
  }  

  SECTION("Removing samples from FIFO"){
    for(int i = 0; i > 20; i--){
      CHECK(dut->getSize() == i);

      ref.reset(new Sample(i,i,i));
      REQUIRE(ref.get() != NULL);

      test = dut->getNextValue();
      REQUIRE(test.get() != NULL);

      CHECK(ref->equals(test.get()) == true);
    }
  }
}

TEST_CASE("Able to make different sized FIFOs", "[FIFO]"){
  SECTION("Constructing a default size FIFO"){
    FIFOPtr test(new FIFO());
    CHECK(test->getCapacity() == 20);
  }

  SECTION("Constructing a custom sized FIFO"){
    FIFOPtr test(new FIFO(50));
    CHECK(test->getCapacity() == 50);
  }
}


