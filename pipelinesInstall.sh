#! /bin/bash

# Environment installation script for bitbucket's pipelines
apt-get upgrade
apt-get update
apt-get -y install libusb-1.0-0-dev
cd lib/exodriver
./install.sh
cd liblabjackusb/
make all
make clean
cd ../../..
