CXX=g++ -I lib/ -I src/ -I test/ -std=c++11

GCC=gcc -I lib/ -I src/ -I test/

# -c skips the linking check and just generates an object file #
CXXFLAGS = -c -g -Wall

LDFLAGS = -lpthread

LJ_FLAGS = -lusb-1.0 -lm

GDB_FLAGS = -ggdb

TEST_OBJ = SampleTest.o FIFOTest.o ModuleTest.o ConfigFileParserTest.o ConfigModuleTest.o ModuleStreamTest.o ConverterTest.o BufferTest.o AverageBufferTest.o ChannelBufferTest.o CompressorTest.o WarningTest.o DiagnosticTest.o FileWriterTest.o

SOURCE_OBJ = Sample.o FIFO.o Module.o ConfigFileParser.o ExceptionLogger.o FileLogger.o ConfigModule.o ModuleStream.o Converter.o Buffer.o AverageBuffer.o ChannelBuffer.o Compressor.o Server.o RedjackInterface.o Redjack.o Saren.o Warning.o Diagnostic.o FileWriter.o

LIB_OBJ = json.o snappy.o snappy-c.o snappy-sinksource.o snappy-stubs-internal.o labjack.o

# Should contain all application make targets only #
all: MainTest TestServer TestRedjack TestFileWriter Redback LabjackRestart

# Should remove application makes and object files #
clean: 
	rm MainTest TestServer TestRedjack TestFileWriter Redback *.o *~ src/*~ test/*~ *.swp src/*.swp test/*.swp *.rbrd *.log LabjackRestart
	
# Some basic rules:
## All object files should be at root level, not in the subdirectories
## Separate and make individual rules for objects as much as possible

# Application make #

MainTest: MainTest.cpp $(TEST_OBJ) $(SOURCE_OBJ) $(LIB_OBJ)
	$(CXX) $< $(TEST_OBJ) $(SOURCE_OBJ) $(LIB_OBJ) -o $@ $(LDFLAGS) $(GDB_FLAGS) $(LJ_FLAGS)

TestServer: TestServer.cpp $(SOURCE_OBJ) $(LIB_OBJ)
	$(CXX) $< $(SOURCE_OBJ) $(LIB_OBJ) -o $@ $(LDFLAGS) $(GDB_FLAGS) $(LJ_FLAGS)

TestRedjack: TestRedjack.cpp $(SOURCE_OBJ) $(LIB_OBJ)
	$(CXX) $< $(SOURCE_OBJ) $(LIB_OBJ) -o $@ $(LDFLAGS) $(GDB_FLAGS) $(LJ_FLAGS)

TestFileWriter: TestFileWriter.cpp $(SOURCE_OBJ) $(LIB_OBJ)
	$(CXX) $< $(SOURCE_OBJ) $(LIB_OBJ) -o $@ $(LDFLAGS) $(GDB_FLAGS) $(LJ_FLAGS)

Redback: Redback.cpp $(SOURCE_OBJ) $(LIB_OBJ)
	$(CXX) $< $(SOURCE_OBJ) $(LIB_OBJ) -o $@ $(LDFLAGS) $(GDB_FLAGS) $(LJ_FLAGS)

LabjackRestart: LabjackRestart.cpp $(SOURCE_OBJ) $(LIB_OBJ)
	$(CXX) $< $(SOURCE_OBJ) $(LIB_OBJ) -o $@ $(LDFLAGS) $(GDB_FLAGS) $(LJ_FLAGS)

# Test file makes #

CompressorTest.o: test/CompressorTest.cpp Compressor.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

ModuleStreamTest.o: test/ModuleStreamTest.cpp ModuleStream.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

ConfigModuleTest.o: test/ConfigModuleTest.cpp ConfigModule.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

ModuleTest.o: test/ModuleTest.cpp Module.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

SampleTest.o: test/SampleTest.cpp Sample.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

FIFOTest.o: test/FIFOTest.cpp FIFO.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

ConfigFileParserTest.o: test/ConfigFileParserTest.cpp ConfigFileParser.o 
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

FileLoggerTest.o: test/FileLoggerTest.cpp FileLogger.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

ConverterTest.o: test/ConverterTest.cpp Converter.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

BufferTest.o: test/BufferTest.cpp Buffer.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

AverageBufferTest.o: test/AverageBufferTest.cpp AverageBuffer.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

ChannelBufferTest.o: test/ChannelBufferTest.cpp ChannelBuffer.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

DiagnosticTest.o: test/DiagnosticTest.cpp Diagnostic.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

WarningTest.o: test/WarningTest.cpp Warning.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)
	
FileWriterTest.o: test/FileWriterTest.cpp FileWriter.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

# Source file makes #

Compressor.o: src/Compressor.cpp src/Compressor.hpp snappy.o json.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

ModuleStream.o: src/ModuleStream.cpp src/ModuleStream.hpp ConfigModule.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

ConfigModule.o: src/ConfigModule.cpp src/ConfigModule.hpp Module.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

Module.o: src/Module.cpp src/Module.hpp FIFO.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

FIFO.o: src/FIFO.cpp src/FIFO.hpp Sample.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

Sample.o: src/Sample.cpp src/Sample.hpp
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

ConfigFileParser.o: src/ConfigFileParser.cpp src/ConfigFileParser.hpp src/ConfigFile.hpp json.o ExceptionLogger.o src/FilePtr.hpp
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

ExceptionLogger.o: src/ExceptionLogger.cpp src/ExceptionLogger.hpp FileLogger.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

FileLogger.o: src/FileLogger.cpp src/FileLogger.hpp src/FilePtr.hpp
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

Converter.o: src/Converter.cpp src/Converter.hpp ConfigModule.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

Buffer.o: src/Buffer.cpp src/Buffer.hpp FIFO.o 
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

AverageBuffer.o: src/AverageBuffer.cpp src/AverageBuffer.hpp Buffer.o 
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

ChannelBuffer.o: src/ChannelBuffer.cpp src/ChannelBuffer.hpp AverageBuffer.o Buffer.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

RedjackInterface.o: src/RedjackInterface.cpp src/RedjackInterface.hpp ChannelBuffer.o 
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS) $(LJ_FLAGS)

Redjack.o: src/Redjack.cpp src/Redjack.hpp ConfigModule.o RedjackInterface.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS) $(LJ_FLAGS)

Server.o: src/Server.cpp src/Server.hpp
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

Saren.o: src/Saren.cpp src/Saren.hpp ModuleStream.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

Warning.o: src/Warning.cpp src/Warning.hpp ExceptionLogger.o ConfigModule.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)
	
Diagnostic.o: src/Diagnostic.cpp src/Diagnostic.hpp ExceptionLogger.o ConfigModule.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)
	
FileWriter.o: src/FileWriter.cpp src/FileWriter.hpp
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

# Library file makes #

json.o: lib/jsoncpp.cpp lib/json.h
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

snappy.o: lib/snappy.cc lib/snappy.h snappy-c.o snappy-sinksource.o snappy-stubs-internal.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

snappy-c.o: lib/snappy-c.cc lib/snappy-c.h lib/snappy.h
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

snappy-sinksource.o: lib/snappy-sinksource.cc lib/snappy-sinksource.h
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

snappy-stubs-internal.o: lib/snappy-stubs-internal.cc lib/snappy-stubs-internal.h lib/snappy-stubs-public.h
	$(CXX) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS)

labjack.o: lib/labjackusb.c lib/labjackusb.h
	$(GCC) $(CXXFLAGS) $< -o $@ $(GDB_FLAGS) $(LJ_FLAGS)
