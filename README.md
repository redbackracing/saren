# README #

## Contribution ##

### Features ###
All new features should be on a different branch to master. As there are no automated methods to control this currently (need Bitbucket Pro for that), any unauthorised commits are reset.

The conditions of acceptance for a new feature are:

* Feature should be made ready for merge via pull request
* Need at least 2 approvals before merge
* Needs to pass Continous Integration checks via Bitbucket pipelines
* The purpose of change has to be established in description
* Code should follow the [C++ Style Guide](https://google.github.io/styleguide/cppguide.html), Note 2 space (not tab) indenting and K&R brace placement

### Bug fixes ###
Bug fixes are done the same way as features, however only require a single approval before merge is allowed.

### Review Process ###
In order of priority:

* Check dynamically allocated memory is freed
* No raw pointers
* Attempt to use references and const where possible
* Code is optimal in space and time
* Check build time isn't overly affected by change, by comparing branch and master builds
* Check code is laid out cleanly and every function has comments

### Makefile Structure ###
The Makefile used in this project should be replaced by a better build system, but till that point, dependencies will be maintained manually. Each file should be separately compiled into an object file (.o) and then linked to create the application, as appropriate. The only exception to this rule currently is MainTest, as the catch main cannot be defined in two places simulataneously.
