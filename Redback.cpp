#include <Saren.hpp>

#include <vector>
#include <iostream>
#include <string>

void consoleApplication(ModuleStream& stream);
int commandtoInt(std::string& iCommand, std::vector<std::string>& oArgs);
std::vector<std::string> split(const std::string& s, char seperator);

enum commands {START = 1, STOP = 2, RECONFIG = 3, EXIT = 4};

int main(int argc, char* argv[]){
  bool testRun = false;
  Saren stream;

  if(argc > 1){
    testRun = true;
  }

  if(testRun){
    stream.start("good");
    stream.stop();  
  } else {
    consoleApplication(stream);
  }

  return 0;
}

void consoleApplication(ModuleStream& stream){
  std::string command;
  std::vector<std::string> args;
  bool running = true;

  while(running){
    std::cout << "Enter command: ";
    getline(std::cin,command);

    switch(commandtoInt(command,args)){
      case START:
        stream.start(args[0]);
        break;

      case STOP:
        stream.stop();
        break;

      case RECONFIG:
        stream.reconfigure(args[0]);
        break;

      case EXIT:
        stream.stop();
        running = false;
        break;

      case -1:
        std::cout << "Invalid input " << command << ":\nUsage [start|stop|reconfig|exit]\nFor [start] and [reconfig] you need\n[config file name without config/ or .rbrc at end]\n" << std::endl;
        break;

      default:
        std::cout << "You've reached uncharted territory" << std::endl;
    }
  }
}

//Converts a iCommand string to a command code as per the enum and
//returns any arguments if applicable
int commandtoInt(std::string& iCommand, std::vector<std::string>& oArgs){
  int result = -1;
  char seperator = 0x20; //ASCII for space
  oArgs = split(iCommand,seperator);

  if(oArgs.size() > 0){
    if(oArgs[0].compare("start") == 0){
    
      if(oArgs.size() > 1 && !oArgs[1].empty()){
        result = 1;  
      }
    
    } else if(oArgs[0].compare("stop") == 0){
      result = 2;
    
    } else if(oArgs[0].compare("reconfig") == 0){
    
      if(oArgs.size() > 1 && !oArgs[1].empty()){
        result = 3;  
      }
    
    } else if(oArgs[0].compare("exit") == 0){
      result = 4;
    }  
  }
  

  if(result != -1){
    oArgs.erase(oArgs.begin());
  } else {
    oArgs.clear();
  }

  return result;
}

std::vector<std::string> split(const std::string& s, char seperator){
  std::vector<std::string> output;

  std::string::size_type prev_pos = 0, pos = 0;

  while((pos = s.find(seperator, pos)) != std::string::npos){
    std::string substring( s.substr(prev_pos, pos-prev_pos) );

    output.push_back(substring);

    prev_pos = ++pos;
  }

  output.push_back(s.substr(prev_pos, pos-prev_pos)); // Last word

  return output;
}